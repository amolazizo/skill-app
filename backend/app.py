from flask import Blueprint
from flask_restful import Api

from resources.PersonResource import PersonResource
from resources.SkillResource import SkillResource
from resources.SkillPersonResource import SkillPersonResource
from resources.DescriptionLevelResource import DescriptionLevelResource
from resources.LinkSkillResource import LinkSkillResource


api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(PersonResource, '/Person')
api.add_resource(SkillResource, '/Skill')
api.add_resource(SkillPersonResource, '/SkillPerson')
api.add_resource(DescriptionLevelResource, '/DescriptionLevel')
api.add_resource(LinkSkillResource, '/LinkSkillResource')

