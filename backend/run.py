from flask import Flask
from pprint import pprint
from flask_cors import CORS
from flask_jwt_extended import JWTManager

def create_skill(config_filename):
    app = Flask(__name__)

    
    app.config.from_object(config_filename)

    #Setup the Flask-JWT-Extended extension
    app.config['JWT_SECRET_KEY'] = 'Circles-skill'
    jwt = JWTManager(app)
    
  
    with app.app_context():
       from app import api_bp
       app.register_blueprint(api_bp, url_prefix='/amal/api')
       
       
       from model import db
       db.init_app(app)
       
       CORS(app)
             
       from resources import PersonResource
       from resources import PersonResource
       from resources import SkillPersonResource
       from resources import DescriptionLevelResource
       from resources import LinkSkillResource
      
       pprint(app.url_map)
       
       return app
   
    
if __name__ == "__main__":
   app = create_skill("config")
   app.run(debug=True)

    
