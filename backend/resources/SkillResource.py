from flask import request, jsonify, current_app as app
from flask_restful import Resource
from model import db, Skill, SkillSchema
from pprint import pprint

skills_schema = SkillSchema(many=True)
skill_schema = SkillSchema()

class SkillResource(Resource):

    @app.route("/amal/api/Skill/<id>", methods=["GET"])
    def get_skill(id):
        skill = Skill.query.get_or_404(id)
        return skill_schema.jsonify(skill)
            
    # Get methode for fetching customers                                                                
    @app.route("/amal/api/Skill", methods=["GET"])
    def get_skills():
        skills = Skill.query.all()
        skills = skills_schema.dump(skills).data
        return skills_schema.jsonify(skills)

    #Post method for creating new skill
    @app.route("/amal/api/Skill", methods=["POST"])
    def post_skill():
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = skill_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422
        skill = Skill.query.filter_by(name_skill=data['name_skill']).first()
               
        if skill is not None:
            return jsonify({'message': 'Skill already exists'})

        
        skill = Skill(
            name_skill=data['name_skill'],
            description_skill=data['description_skill'],

        )       
        db.session.add(skill)
        db.session.commit()

        result = skill_schema.dump(skill).data

        return jsonify({ "status": 'success', 'data': result }), 201

    #Put method for updating skill
    @app.route("/amal/api/Skill/<id>", methods=["PUT"])
    def put_skill(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = skill_schema.load(json_data)
        if errors:
            return errors, 422
        skill = Skill.query.get_or_404(id)
        if not skill:
            return jsonify({'message': 'Skill does not exist'}), 400
        skill.name_skill=json_data['name_skill']
        skill.description_skill=json_data['description_skill']

        
               
        db.session.commit()
        result = skill_schema.dump(skill).data

        return jsonify({ "status": "success"}), 204
    
    # Delete method for deleting skill
    @app.route("/amal/api/Skill/<id>", methods=["DELETE"])
    def delete_skill(id):
        skill = Skill.query.get_or_404(id)
        db.session.delete(skill)
        db.session.commit()
        return jsonify({"message" : "success"}), 200
