from flask import request, jsonify, current_app as app
from flask_restful import Resource
from model import db, LinkSkill, Skill, LinkSkillSchema
from pprint import pprint

linkSkills_schema = LinkSkillSchema(many=True)
linkSkill_schema = LinkSkillSchema()

class LinkSkillResource(Resource):

    # Get methode for fetching LinkSkill id
    @app.route("/amal/api/LinkSkill/<id>", methods=["GET"])
    def get_linkSkill(id):
        linkSkill = LinkSkill.query.get_or_404(id)
        return linkSkill_schema.jsonify(linkSkill)

    # Get methode for fetching link of skill
    @app.route("/amal/api/LinkSkill", methods=["GET"])
    def get_linkSkills():
        linkSkills = LinkSkill.query.all()
        linkSkills = linkSkills_schema.dump(linkSkills).data
        return linkSkills_schema.jsonify(linkSkills)

    # Post method for creating new linkSkill
    @app.route("/amal/api/LinkSkill", methods=["POST"])
    def post_linkskill():
        json_data = request.get_json(force=True)
        if not json_data:
            return jsonifly({'message': 'No inpus data provided'}), 400
        # Validate and deserialise input
        data, errors = linkSkill_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422

        id_link_lower = Skill.query.filter_by(id=data['id_link_lower']).first()
        if not id_link_lower:
            return jsonify({'status': 'error', 'message': ' Skill not found'})

        id_link_upper = Skill.query.filter_by(id=data['id_link_upper']).first()
        if not id_link_upper:
            return jsonify({'status': 'Skill not found'}), 400


        linkSkill = LinkSkill(
            id_link_upper=data['id_link_upper'],
            id_link_lower=data['id_link_lower']
            )

        db.session.add(linkSkill)
        db.session.commit()

        result = linkSkill_schema.dump(linkSkill).data

        return jsonify({ "status": 'success', 'data': result }), 201


    #Put method for updating linkSkill
    @app.route("/amal/api/LinkSkill/<id>", methods=["PUT"])
    def put_linkSkill(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = linkSkill_schema.load(json_data)
        if errors:
            return errors, 422
        linkSkill = LinkSkill.query.get_or_404(id)
        if not linkSkill:
            return jsonify({'message': 'link Skill does not exist'}), 400
        linkSkill.id_link_lower=json_data['id_link_lower']
        linkSkill.id_link_upper=json_data['id_link_upper']
        
        db.session.commit()

        result = linkSkill_schema.dump(linkSkill).data
        return jsonify({ "status": 'success', 'data': result }), 204
    
    # Delete method for deleting link of skill
    @app.route("/amal/api/LinkSkill/<id>", methods=["DELETE"])
    def delete_linkSkill(id):
        linkSkill = LinkSkill.query.get_or_404(id)
        
        db.session.delete(linkSkill)
        db.session.commit()

        return jsonify({ "message" : "success"}), 200
