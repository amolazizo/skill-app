from flask import request, jsonify, url_for, redirect,  current_app as app
from flask_restful import Resource
from model import db, Person, PersonSchema
from pprint import pprint
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)



persons_schema = PersonSchema(many=True)
person_schema = PersonSchema()


class PersonResource(Resource):

    # Get methode for person  with google_id
    @app.route("/amal/api/Person/<id_google>", methods=["GET"])
    def get_person(id_google):
        print("looking for person with google id: " + id_google)
        person = Person.query.filter_by(id_google=id_google).first_or_404()
        return person_schema.jsonify(person)

    # Get methode for person  with google_id
    @app.route("/amal/api/Person/<id_google>", methods=["POST"])
    def login_person(id_google):
        person = Person.query.filter_by(id_google=id_google).first_or_404()
        print("looking for employee with google id: " + id_google)
        if person is not None:
            
            # Pass  Person object  directly to the create_access_token method.
            #This will allow us to access
            # the properties of this object  and get the identity of this object
            access_token = create_access_token(identity=person.id_google, fresh=True)
            refresh_token = create_refresh_token(person.id_google)
            print("token:" + access_token + "refresh_token:" +refresh_token )
            res = jsonify(access_token), 200
        else:
            res = jsonify({"Error":"Invalid Credentials "}), 401
        return res


    # Get methode for fetching persons
    @app.route("/amal/api/Person", methods=["GET"])
    def get_persons():
        persons = Person.query.all()
        persons = persons_schema.dump(persons).data
        return persons_schema.jsonify(persons)
    
    #Post method for creating new person
    @app.route("/amal/api/Person", methods=["POST"])
    def post_person():
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = person_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422
        
        person = Person.query.filter_by(id_google=data['id_google']).first()
        if person is not None:
            return jsonify({'message': 'Person already exists'}), 200
        person = Person(
            first_name=json_data['first_name'],
            last_name=json_data['last_name'],
            email_person=json_data['email_person'],
            is_admin=json_data['is_admin'],
            id_google=json_data['id_google'],
            img_url=json_data['img_url'],
            
        )
        db.session.add(person)
        db.session.commit()
        
        result = person_schema.dump(person).data

        return jsonify({ "status": 'success', 'data': result }), 201

              


    #Put method for updating person
    @app.route("/amal/api/Person/<id_google>", methods=["PUT"])
    def put_person(id_google):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = person_schema.load(json_data)
        if errors:
            return errors, 422
        person = Person.query.get_or_404(id_google)
        if not person:
            return jsonify({'message': 'Person does not exist'}), 400
        person.first_name=json_data['first_name']
        person.last_name=json_data['last_name']
        person.email_person=json_data['email_person']
        person.is_admin=json_data['is_admin']
        person.id_google=json_data['id_google']
        person.img_url=json_data['img_url']

        db.session.commit()

        result = person_schema.dump(person).data
        return jsonify({ "status": 'success', 'data': result }), 200
    
    # Delete method for deleting person
    @app.route("/amal/api/Person/<id_google>", methods=["DELETE"])
    def delete_person(id_google):
       person = Person.query.get_or_404(id_google)
       db.session.delete(person)
       db.session.commit()

       return jsonify({ "Message" : "success"}), 200


class PersonId(Resource):
    @app.route("/amal/api/PersonId/<id>")
    def get_personId(id):
        personId = Person.query.get_or_404(id)
        return person_schema.jsonify(personId)

