from flask import request, jsonify, current_app as app
from flask_restful import Resource
from model import db, DescriptionLevel, Skill, DescriptionLevelSchema


descriptionLevels_schema = DescriptionLevelSchema(many=True)
descriptionLevel_schema = DescriptionLevelSchema()

class DescriptionLevelResource(Resource):

    @app.route("/amal/api/DescriptionLevel/<id>", methods=["GET"])
    def get_descriptionLevel(id):
        descriptionLevel = DescriptionLevel.query.get_or_404(id)
        return descriptionLevel_schema.jsonify(descriptionLevel)
    
    # Get methode for fetching description of level
    @app.route("/amal/api/DescriptionLevel", methods=["GET"])
    def get_descriptionLevels():
        descriptionLevels = DescriptionLevel.query.all()
        descriptionLevels = descriptionLevels_schema.dump(descriptionLevels).data
        return descriptionLevels_schema.jsonify(descriptionLevels)

    # Post method for creating new decsriptionLevel
    @app.route("/amal/api/DescriptionLevel", methods=["POST"])
    def post_descriptionLevel():
        json_data = request.get_json(force=True)
        if not json_data:
            return jsonify({'message': 'No input data provided'}), 400
        data, errors = descriptionLevel_schema.load(json_data)
        if errors:
            return errors, 422
        id_skill = Skill.query.filter_by(id=data['id_skill']).first()
        if not id_skill:
            return jsonify({'status':  'Skill  not found'}), 400
        descriptionLevel = DescriptionLevel(
            level_description=data['level_description'],
            level_number=data['level_number'],
            id_skill=data['id_skill']
            )

        db.session.add(descriptionLevel)
        db.session.commit()

        result = descriptionLevel_schema.dump(descriptionLevel).data
        return jsonify({ 'status': 'success ', 'data': result}), 201

    #Put method for updating descriptionLevel
    @app.route("/amal/api/DescriptionLevel/<id>", methods=["PUT"])
    def put_descriptionLevel(id):
        json_data = request.get_json(force=True)
        if not json_data:
            return jsonify({'message': 'No input data provided'}), 400
        data, errors = descriptionLevel_schema.load(json_data)
        if errors:
            return errors, 422
        descriptionLevel = DescriptionLevel.query.get_or_404(id)
        if not descriptionLevel:
            return jsonify({'message': 'Description of level  does not exist'}), 400
        descriptionLevel.level_description=json_data['level_description']
        descriptionLevel.level_number=json_data['level_number']
        descriptionLevel.id_skill=json_data['id_skill']
       
        db.session.commit()

        result = descriptionLevel_schema.dump(descriptionLevel).data
        return jsonify({ "status": "success.",  "data": result }), 204

    #Delete method for deleting Description of level
    @app.route("/amal/api/DescriptionLevel/<id>", methods=["DELETE"])
    def delete_descriptionLevel(id):
        descriptionLevel = DescriptionLevel.query.get_or_404(id)
        db.session.delete(descriptionLevel)
        db.session.commit()

        return jsonify({"message" : "success"}), 200
