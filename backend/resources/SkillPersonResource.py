from flask import request, jsonify, current_app as app
from flask_restful import Resource
from model import db, Person, Skill, SkillPerson, DescriptionLevel, SkillPersonSchema
from pprint import pprint

skillPersons_schema = SkillPersonSchema(many=True)
skillPerson_schema = SkillPersonSchema()

class SkillPersonResource(Resource):

    # Get methode for fetching My skill with  id_person
    @app.route("/amal/api/SkillPerson/<id_person>", methods=["GET"])
    def get_skillPerson(id_person):
        print("looking for person with id:" + id_person)
        skillPerson = SkillPerson.query.filter(SkillPerson.id_person==id_person).all()
        skillPerson = skillPersons_schema.dump(skillPerson).data
       # skillPerson = SkillPerson.query.filter(SkillPerson.id_person==id_person).all()
        return skillPersons_schema.jsonify(skillPerson)

    @app.route("/amal/api/SkillPerson", methods=["GET"])
    def get_skillPersons():
        skillPersons = SkillPerson.query.group_by(SkillPerson.id_person).all()
        skillPersons = skillPersons_schema.dump(skillPersons).data
        return skillPersons_schema.jsonify(skillPersons)

    # Post method for creating new skillPerson
    @app.route("/amal/api/SkillPerson", methods=["POST"])
    def post_skillPerson():
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No inpus data provided'}, 400
        # Validate and deserialise input
        data, errors = skillPerson_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422
        id_skill = Skill.query.filter_by(id=data['id_skill']).first()
        if not id_skill:
            return jsonify({'status': 'error', 'message': 'Skill not found'})
        skillPerson = SkillPerson(
            level=data['level'],
            my_skill_description=data['my_skill_description'],
            id_person=data['id_person'],
            id_skill=data['id_skill']
           # id_descriptionLevel=data['id_descriptionLevel']
            )
        
        db.session.add(skillPerson)
        db.session.commit()

        result = skillPerson_schema.dump(skillPerson).data

        return jsonify({ "status": 'success', 'data': result }), 201


    #Put method for updating skillPeron
    @app.route("/amal/api/SkillPerson/<id>", methods=["PUT"])
    def put_skillPeron(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = skillPerson_schema.load(json_data)
        if errors:
            return errors, 422
        skillPerson = SkillPerson.query.get_or_404(id)
        if not skillPerson:
            return jsonify({'message': 'Skill for person does not exist'}), 400
        skillPerson.level=json_data['level']
        skillPerson.my_skill_description=json_data['my_skill_description']
        skillPerson.id_person=json_data['id_person']
        skillPerson.id_skill=json_data['id_skill']
        #skillPerson.id_descriptionLevel=json_data['id_descriptionLevel']
        
        db.session.commit()

        result = skillPerson_schema.dump(skillPerson).data
        return jsonify({ "status": 'success', 'data': result }), 204
    
    # Delete method for deleting skilPerson
    @app.route("/amal/api/SkillPerson/<id>", methods=["DELETE"])
    def delete_skillPersont(id):
        skillPerson = SkillPerson.query.get_or_404(id)
        db.session.delete(skillPerson)
        db.session.commit()

        return jsonify({ "message" : "success"}), 204





class MySkill(Resource):

    @app.route("/amal/api/MySkill/<id>")
    def get_mySkill(id):
        mySkill = SkillPerson.query.get_or_404(id)
        return skillPerson_schema.jsonify(mySkill)


        
