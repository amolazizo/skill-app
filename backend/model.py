from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship, backref


shma= Marshmallow()
db = SQLAlchemy()

# Create a Person  table.
class Person(db.Model):
   __tablename__ = 'person'
   id = db.Column(db.Integer, primary_key=True)
   first_name = db.Column(db.String(60), nullable=False)
   last_name = db.Column(db.String(60), nullable=False)
   email_person = db.Column(db.String(60), nullable=False)
   is_admin = db.Column(db.Boolean(), default=False)
   id_google = db.Column(db.String(225), nullable=False)
   img_url = db.Column(db.String(1000), nullable=False)
  
   
   #initializing attributes for the Person table
   def __init__(self, first_name, last_name, email_person, is_admin, id_google, img_url):
      self.first_name = first_name
      self.last_name = last_name
      self.email_person = email_person
      self.is_admin = is_admin
      self.id_google = id_google
      self.img_url = img_url

# Create a Skill table.
class Skill(db.Model):
   __tablename__ = 'skill'
   id = db.Column(db.Integer, primary_key=True)
   name_skill = db.Column(db.String(60), nullable=False)
   description_skill= db.Column(db.String(1000), nullable=False)


    #initializing attributes for  Skills table
   def __init__(self, name_skill, description_skill):
      self.name_skill = name_skill
      self.description_skill = description_skill


# Create table linkSkill
class LinkSkill(db.Model):
   __tablename__ = 'linkSkill'
   id = db.Column(db.Integer, primary_key=True)
   id_link_upper = db.Column(db.Integer, db.ForeignKey('skill.id'), nullable=False)
   id_link_lower = db.Column(db.Integer, db.ForeignKey('skill.id'), nullable=False)
   
   link_upper = db.relationship(Skill, foreign_keys='[LinkSkill.id_link_upper]')
   link_lower = db.relationship(Skill, foreign_keys='[LinkSkill.id_link_lower]')

   #initializing attributes for linkSkill table
   def __init__(self, id_link_upper, id_link_lower):
      self.id_link_upper = id_link_upper
      self.id_link_lower = id_link_lower


#Create a descriptionLevel table
class DescriptionLevel(db.Model):
   __tablename__ = 'descriptionLevel'
   id = db.Column(db.Integer, primary_key=True)
   level_number=db.Column(db.Integer, nullable=False)
   level_description = db.Column(db.String(1000), nullable=False)
   id_skill = db.Column(db.Integer, db.ForeignKey('skill.id'), nullable=False)

   skill = db.relationship(Skill, backref=backref('skill', uselist=True))

   #initializing attributes for the descriptionLevel table
   def __init__(self, level_description, level_number, id_skill):
      self.level_description = level_description
      self.level_number = level_number
      self.id_skill = id_skill


      
# Create a  skillPerson  table.
class SkillPerson(db.Model):
   __tablename__ = 'skillPerson'
   id = db.Column(db.Integer, primary_key=True)
   level = db.Column(db.Integer, nullable=True)
   my_skill_description = db.Column(db.String(1000), nullable=True)
   id_person = db.Column(db.Integer, db.ForeignKey('person.id'), nullable=False)
   id_skill = db.Column(db.Integer, db.ForeignKey('skill.id'), nullable=False)
   #id_descriptionLevel = db.Column(db.Integer, db.ForeignKey('descriptionLevel.id'), nullable=True)

   person = db.relationship(Person, backref=backref('skillPerson', uselist=True))
   skill= db.relationship(Skill, backref=backref('skillPerson', uselist=True))
   #descriptionLevel= db.relationship(DescriptionLevel, backref=backref('skillPerson', uselist=True))


   #initializing attributes for the SkillPerson table
   def __init__(self, level, my_skill_description, id_person, id_skill):
      self.level = level
      self.my_skill_description = my_skill_description
      self.id_person = id_person
      self.id_skill = id_skill
      #self.id_descriptionLevel = id_descriptionLevel

#convert data types from the Person  table(object) to native Python types.
class PersonSchema(shma.Schema):
    id = fields.Integer()
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)
    email_person = fields.String(required=True)
    is_admin = fields.Boolean()
    id_google = fields.String(required=True)
    img_url = fields.String(required=True)

    skillPerson = fields.Nested("SkillPersonSchema")


#convert data types from the Skill table(object) to native Python types.
class SkillSchema(shma.Schema):
    id = fields.Integer()
    name_skill = fields.String(required=True)
    description_skill = fields.String(required=False)


    skillPerson = fields.Nested("SkillPersonSchema")
    linkSkill = fields.Nested("LinkSkillSchema")
    descriptionLevel = fields.Nested("DescriptionLevelSchema")


#convert data types from the linkSkill table(object) to native Python types.
class LinkSkillSchema(shma.Schema):
    id = fields.Integer()
    id_link_upper = fields.Integer(required=True)
    id_link_lower = fields.Integer(required=True)

    link_upper = fields.Nested("SkillSchema")
    link_lower = fields.Nested("SkillSchema")


#convert data types from the DescriptionLevel  table(object) to native Python types.
class DescriptionLevelSchema(shma.Schema):
    id = fields.Integer()
    level_description = fields.String(required=True)
    level_number = fields.Integer(required=True)
    id_skill = fields.Integer(required=True)

    skill = fields.Nested("SkillSchema")
    skillPerson = fields.Nested("SkillPersonSchema")

    
#convert data types from skillPerson table to native Puthon types. 
class SkillPersonSchema(shma.Schema):
       id = fields.Integer()
       level = fields.Integer(required=False)
       my_skill_description = fields.String(required=False)
       id_person = fields.Integer(required=True)
       id_skill = fields.Integer(required=True)
       #id_descriptionLevel = fields.Integer(required=False)

       person = fields.Nested("PersonSchema")
       skill = fields.Nested("SkillSchema")
       #descriptionLevel = fields.Nested("DescriptionLevelSchema")

