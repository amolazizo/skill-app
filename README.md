Skill Application is an internal application that allows to manage the different skills of employees within the company to facilitate the task of the recruiters to easily find the profile that corresponds to the experiences requested by the clients of the company. 

The technologies and tools used to realize this application are as follows:
---------------------------------------------------------------------------

Python, Flask, Restful API, SQLAlchemy, Maria DB, Node.js, React JS, HTML, CSS, Javascript, Git and Bootstrap.
