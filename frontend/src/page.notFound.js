import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class PageNotFound extends Component {

    render() {
return (
<div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
          <div>
                 <h1>Page not found</h1><br>
                </br>
                <p>Clique <Link to='/'className="van-link">here</Link> to redirected to the homepage!</p>
          </div>
        
          </div>
        </div>
            </div>
)
    }

}

export default PageNotFound;