import React, { Component } from 'react';
import AuthService from './auth.service';

export default function WithAuth(AuthComponent)
{
    const Auth = new AuthService('/amal/api'); // API server domain
    return class AuthWrapped extends Component {

        constructor() {
            super();
            this.state = {
                person: null,
            }
        }

        componentWillMount() {
            if (!Auth.loggedIn()) {
                this.props.history.replace('/register.person')
            }
            else {
                try {
                    const profile = Auth.getProfile()
                    this.setState({
                        person: profile
                    })  
                   
                }
                catch(error){
                    Auth.logout()
                    this.props.history.replace('/register.person')

                }
            }
        }
        render() {
            if (this.state.person ) {
                return (
                    <AuthComponent history={this.props.history} person={this.state.person} location={this.state.location}/>
                )
            }
            else {
                return null
            }
        }

    }



}