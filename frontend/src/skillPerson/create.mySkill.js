import React, { Component } from 'react';
import axios from 'axios';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormErrors from '../form.errors';
import WithAuth from '../withAuth';
import { Link } from 'react-router-dom';
import VerticalNavBarre from '../vertical.nav.barre';

class CreateMySkill extends Component {

    constructor(props) {
        super(props);

        this.state = {
            skills: [],
            selectedSkillId: -1,
            id_descriptionLevel: null,
            level: 0,
            my_skill_description: '',
            formErrors: { level: 0, selectedSkillId: -1 },
            levelValid: false,
            skillValid:false,
            formValid: false,
            id_person:0

        };
    }

    componentWillMount() {
        console.log(this.props.person.identity)
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("id_person:" + response.data.id);
                this.setState({
                    id_person: response.data.id,
                });

            })
            .catch(function (error) {
                console.log(error);

            })

    }




    componentDidMount() {
        axios.get("/amal/api/Skill")
            .then(response => {
                console.log(response.data);
                this.setState({ skills: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,

        });
    };

    CreateMySkill() {

        const mySkill = {
            my_skill_description: this.state.my_skill_description,
            level: this.state.level,
            id_skill: this.state.selectedSkillId,
            id_person: this.state.id_person
        };

        const url = `${API_URL}/SkillPerson`;

        axios.post(url, mySkill)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.mySkillTable');


            }).catch((err) => {
                console.log(err);
            });
    }


    handleMySkillInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }


    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let skillValid = this.state.skillValid;
        let levelValid = this.state.levelValid;

        switch (fieldName) {
            case 'level':
                levelValid = value.length <= 1;
                fieldValidationErrors.level = levelValid ? '' : 'must to be between 0-4.';
                break;
            case 'selectedSkillId':
               skillValid = value.length >= 1;
                fieldValidationErrors.selectedSkillId = skillValid ? '' : ' This field is required ';
                break;
            /*  case 'selectedSkillId':
                  skillValid = value.length >= -1;
                  fieldValidationErrors.selectedSkillId= skillValid ? '' : '  is required ';
                  break;  */
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            levelValid: levelValid,
            //  skillValid: skillValid,
            skillValid: skillValid
        },
            this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.levelValid && this.state.skillValid });
    }

    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }


    handleCreateMySkill = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: 'You are sure ?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.CreateMySkill(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };


    render() {

        const skillsOptions = this.state.skills.map(function (skill) {
            return (
                <option key={skill.id} value={skill.id}>{skill.name_skill}</option>
            );
        });

        return (
            <>
            <div className="col-lg-3 bg-info rounded">
            <VerticalNavBarre />
            </div>
            <div className="col-lg-9 pl-4">
            <div className="jumbotron ">
                <FormErrors formErrors={this.state.formErrors} />

                <h6 className="border-bottom border-gray">Add my  skill </h6>
                <div className="pt-3"></div>


                <form onSubmit={this.handleCreateMySkill}>

                    <table className='table table-borderless'>
                        <tbody>
                         
                            <tr>
                                <td className="text-muted  detail-level"><strong className="text-gray-dark">Skill name:</strong></td>
                                <td className="person-col">
                                    <select
                                        onChange={(event) => this.handleMySkillInput(event)}
                                        className='form-control'
                                        name="selectedSkillId"
                                        value={this.state.selectedSkillId}>
                                        <option value="-1">Select skill...</option>
                                        {skillsOptions}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td className="text-muted detail-level"><strong className="text-gray-dark">Level:</strong></td>
                                <td className="small person-col">
                                    <input type="number"
                                        min="0"
                                        max="4"
                                        placeholder="Enter number level"
                                        className="form-control"
                                        name="level"
                                        value={this.state.level}
                                        onChange={(event) => this.handleMySkillInput(event)}
                                    />

                                </td>

                            </tr>





                            <tr>
                                <td className="text-muted"><strong className="d-block text-gray-dark">My description of skill :</strong></td>
                                <td className="small">
                                    <p></p>
                                    <textarea
                                        id="comment" rows="8"
                                        placeholder="Enter the description of skill ..."
                                        className="form-control"
                                        name="my_skill_description"
                                        value={this.state.my_skill_description}
                                        onChange={(event) => this.handleMySkillInput(event)}

                                    />
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td className="float-right">
                                    <button
                                        type="submit"
                                        className="btn btn-info"
                                        disabled={!this.state.formValid}>Add skill</button>
                                </td>
                                <td className="float-right px-2">
                                <Link to="/empty.mySkillTable" className="btn btn-info">
                                        Return
                                </Link>
                                </td> 


                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            </div>
            </>

        );
    }


}
export default WithAuth(CreateMySkill);

