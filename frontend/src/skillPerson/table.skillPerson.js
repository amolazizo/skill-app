import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import axios from 'axios';
import readMoreImg from '../pictures/read-more-grey.png'
import { Link } from 'react-router-dom';



class TableSkillPerson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            allSkills: []
        }
    }



    componentDidMount() {
        console.log(this.props.skill_persons.id_person);
        console.log('Should be id',this.props.skill_persons.id_person)
        axios.get('/amal/api/SkillPerson/' + this.props.skill_persons.id_person)
            .then(response => {
                console.log('DATA',response.data);
                this.setState({ allSkills: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }


    render() {

        return (
            <>
<tr>
                    <td>
                    {this.state.allSkills.map(skillsOfPerson => {
                         return (  <li>{skillsOfPerson.level}</li>
                            )})}
            
                        
                    </td>
                    <td className="align-middle">
                        <Link to={"detail.skillPerson/" + this.props.skill_persons.id_person} className="align-middle"><img src={readMoreImg} alt="readmore-img" width="20" height="20" className="button-border" /></Link>&nbsp;&nbsp;
                    </td>

                </tr>
            </>
        );
    }
}

export default TableSkillPerson;
