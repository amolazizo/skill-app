
import React, { Component } from 'react';
import axios from 'axios';
import NotAdminPage from '../not.admin.page';
import WithAuth from '../withAuth';
import ListSkillPerson from './list.skillPerson';
import VerticalNavBarre from '../vertical.nav.barre';



class EmptySkillPersonTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
            skillPersons: []

        }

    }

    componentDidMount() {
        console.log(this.props.person.identity)
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("Admin :" + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                });
            })
        axios.get('/amal/api/SkillPerson')
            .then(response => {
                console.log(response.data);
                this.setState({ skillPersons: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    isAdmin() {
        const totalSkillPersons = this.state.skillPersons.length;
        if (this.state.is_admin === true) {
            if (totalSkillPersons === 0) {
                return (
                    <div className="container">
                        <div className="jumbotron mt-5">
                            <div className="col-xl-8 mx-auto">
                                <div>
                                    <h6>No data in database!</h6>
                                </div>

                            </div>
                        </div>
                    </div>
                )
            }
            else {
                return (
                    

                    <ListSkillPerson />

                )

            }

        }

        else {
            return (
                < NotAdminPage />
            )
        }
    }

    render() {

        return (
            <div className="row">
            <div className="col-lg-3 bg-info rounded">
            <VerticalNavBarre />
            </div>
            <div className="col-lg-9 pl-4">
                {this.isAdmin()}
            </div>
            </div>
        )


    }
}

export default WithAuth(EmptySkillPersonTable)
