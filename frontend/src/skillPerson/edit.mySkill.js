import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { Link } from 'react-router-dom';
import VerticalNavBarre from '../vertical.nav.barre';



class EditMySkill extends Component {

    constructor(props) {
        super(props);

        this.state = {
            level: 0,
            my_skill_description: "",
            selectedSkillId: 1,
            id_person: 0,
            name_skill: "",
            skills: []
        }
    }

    componentWillMount() {

        axios.get("/amal/api/Skill")
            .then(response => {
                this.setState({ skills: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });


    }

    componentDidMount() {
        console.log(this.props.match.params.id);
        axios.get('/amal/api/MySkill/' + this.props.match.params.id)
            .then(res => {
                console.log(res.data);
                console.log("level :" + res.data.level)
                this.setState({
                    level: res.data.level,
                    my_skill_description: res.data.my_skill_description,
                    id_person: res.data.id_person,
                    id_skill: res.data.selectedSkillId,
                    name_skill: res.data.skill.name_skill
                });



            })
            .catch(function (error) {
                console.log(error);
            })
    }


    onChangeSkillName = (e) => {
        console.log("got event value: " + JSON.stringify(e.target.value))
        e.preventDefault()
        this.setState({
            selectedSkillId: e.target.value
        });
    }


    onChangeMySkillDescription = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            my_skill_description: e.target.value
        });
    }

    onChangeMyLevel = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            level: e.target.value
        });
    }

    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,
        });
    };

    editMySkill() {

        const mySkill = {
            level: this.state.level,
            my_skill_description: this.state.my_skill_description,
            id_skill: this.state.selectedSkillId,
            id_person: this.state.id_person
        };

        const url = `${API_URL}/SkillPerson/` + this.props.match.params.id;

        axios.put(url, mySkill)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.mySkillTable');

            }).catch((err) => {
                console.log(err);
            });
    }

    handleEditMySkill = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: ' You want to save this change?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.editMySkill(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };

    render() {

        const skillOptions = this.state.skills.map(function (skill) {
            return (
                <option key={skill.id} value={skill.id}>{skill.name_skill}</option>
            );
        });

        return (
            <>
                <div className="col-lg-3 bg-info rounded">
                    <VerticalNavBarre />
                </div>
                <div className="col-lg-9 pl-4">
                    <div className="jumbotron  mt-1">
                        <h6 className="border-bottom border-gray">Make changes to this skill:</h6>
                        <div className="pt-3"></div>
                        <form onSubmit={this.handleEditMySkill}>

                            <div className="row skill-margin">
                                <div className="col-xl-4 text-muted"><strong className="text-gray-dark">Skill name:</strong>
                                </div>
                                <div className="small col-xl-7">
                                    <select
                                        onChange={this.onChangeSkillName}
                                        className='form-control'
                                        required="required"
                                        value={this.state.selectedSkillId}>
                                        <option value="1">{this.state.name_skill}</option>
                                        {skillOptions}
                                    </select>
                                </div>
                                <div className="col-xl-1"></div>
                            </div>

                            <div className="row skill-margin">
                                <div className="text-muted col-xl-4"><strong className="d-block text-gray-dark">My level of skill:</strong>
                                </div>
                                <div className="small col-xl-7">
                                    <input type="number"
                                        min="0"
                                        max="4"
                                        required="required"
                                        placeholder="Enter number level"
                                        className="form-control"
                                        name="level_number"
                                        value={this.state.level}
                                        onChange={this.onChangeMyLevel}
                                    />
                                </div>
                                <div className="col-xl-1"></div>
                            </div>


                            <div className="row skill-margin">
                                <div className="text-muted col-xl-4"><strong className="d-block text-gray-dark">My description:</strong>
                                </div>
                                <div className="small col-xl-7">
                                    <textarea
                                        className="formcontrol" id="comment" rows="8"
                                        required="required"
                                        value={this.state.my_skill_description}
                                        onChange={this.onChangeMySkillDescription}

                                    />
                                </div>
                                <div className="col-xl-1"></div>
                            </div>


                            <div className="row skill-margin">
                                <div className="col-xl-11 float-righ">
                                    <div className="float-right px-2">
                                        <button
                                            type="submit"
                                            className="btn btn-info"
                                            required="required"
                                        >Update my skill</button>
                                    </div>
                                    <div className="float-right px-2">
                                        <Link to="/empty.mySkillTable" className="btn btn-info">
                                            Return
                                </Link>
                                    </div>
                                </div>
                                <div className="col-xl-1"></div>

                            </div>
                        </form >
                    </div >
                </div>
            </>


        )
    }

}




export default EditMySkill