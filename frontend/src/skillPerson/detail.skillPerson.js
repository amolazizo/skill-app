import React, { Component } from 'react';
import axios from 'axios';
import { Button } from 'react-bootstrap';



class DetailSkillPerson extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allSkills: [],
            fist_name: "",
            last_name: "",
            img_url: "",

        }

    }

    componentDidMount() {
        console.log(this.props.match.params.id_person);
        axios.get('/amal/api/SkillPerson/' + this.props.match.params.id_person)
            .then(response => {
                console.log(response.data)
                this.setState({
                    allSkills: response.data
                });

            })

            .catch(function (error) {
                console.log(error);
            });


        axios.get("/amal/api/PersonId/" + this.props.match.params.id_person)
            .then(response => {
                this.setState({
                    first_name: response.data.first_name,
                    last_name: response.data.last_name,
                    img_url: response.data.img_url
                });
            })
            .catch(function (error) {
                console.log(error);
            });

    }


    render() {

        const { allSkills } = this.state;

        const tabSkills = allSkills.map(skillsOfPerson => {
            return (
                <table className='table table-borderless bg-light rounded'>
                    <tbody className="">
                        <tr>
                            <td className="text-muted detail-level"><b>Skill name :</b></td>
                            <td className="large"><b>
                             {skillsOfPerson.skill.name_skill}
                             </b>
                            </td>
                        </tr>
                        <tr>
                            <td className="text-muted detail-level ">Level of skill:</td>
                            <td>
                                {skillsOfPerson.level}
                            </td>
                        </tr>
                        <tr>
                            <td className="text-muted detail-level">Description of consultant:</td>
                            <td className="small">
                                {skillsOfPerson.my_skill_description}
                            </td>
                        </tr>

                    </tbody>
                </table>
            )
        });


        return (
            <div className="jumbotron  mt-0">

                <div>
                    <td className="text-muted">
                        <img src={this.state.img_url} alt="home-img" width="80" height="80" className="rounded" />&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>
                    <td><span className="text-name  mb-0">{this.state.last_name}  {this.state.first_name}</span></td>

                </div>
                <br></br>
                <div>

                    {tabSkills}

                </div>

                <div className="mt-5">
                    <Button href="/empty.skillPersonTable" variant="info" size="bg" block>
                        Return
                                </Button>
                </div>
            </div>

        )
    }
}
export default DetailSkillPerson
