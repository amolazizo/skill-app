
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import axios from 'axios';
import WithAuth from '../withAuth';
import ListMySkill from './list.mySkill';
import VerticalNavBarre from '../vertical.nav.barre';


class EmptyMySkillTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
            id_person: 0,
            allMySkills: []

        }

    }

    componentWillMount() {
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("Admin :" + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                    id_person: response.data.id
                })
                console.log(this.state.id_person);
                axios.get('/amal/api/SkillPerson/' + this.state.id_person )
                .then(response => {
                console.log(response.data);
                this.setState({
                     allMySkills: response.data
                      });
            })
        })
            .catch(function (error) {
                console.log(error);

            })
    }


    myList() {
        console.log(this.state.id_person);
        const totalMySkills = this.state.allMySkills.length;
            if (totalMySkills === 0) {
                return (
                    <>
                        <div className="jumbotron mt-5">
                            <div className="col-xl-8 mx-auto">
                                <div>
                                    <h3>No data for your skills!</h3>
                                    <br></br>
                                    <p>Please insert data in database.</p>
                                </div>

                            </div>
                        </div>
                        <div>
                            <footer>
                                <nav className="navbar navbar-expand-lg navbar-dark bg-info rounded">
                                    <div
                                        className="collapse navbar-collapse justify-content-md-center"
                                        id="navbarsExample10"
                                    >
                                        <ul className="navbar-nav">
                                            <li className="nav-item">

                            
                                                <Link to="/create.mySkill" className="btn mb-0 text-white"><h6> Add my skill</h6>
                                                   
                                                 </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </footer>
                        </div>
                        </>
                )
            }
            else {
                return (

                    <ListMySkill />
                )

            }
        }

    render() {

        return (
            <>
            <div className="col-lg-3 bg-info rounded">
            <VerticalNavBarre />
            </div>
            <div className="col-lg-9 pl-4">
                {this.myList()}
            </div>
            </>
        )


    }
}

export default withRouter(WithAuth(EmptyMySkillTable));
