import React, { Component } from 'react';
import axios from 'axios';
import WithAuth from '../withAuth';
import NotAdminPage from '../not.admin.page';
import { Nav, Button } from 'react-bootstrap';
import addImg from '../pictures/add-img-grey.png';
import ImgDesc from '../pictures/img-desc.png';

import Select from 'react-select';
//import TableSkillPerson from './table.skillPerson';
//import Pagination from '../pagination';
//import ReactTable from 'react-table';
//import { Table } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter, selectFilter, Comparator } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import readMoreImg from '../pictures/read-more-grey.png'
import { Link } from 'react-router-dom';
class ListSkillPerson extends Component {

  constructor(props) {
    super(props);
    this.state = {
      allSkillPersons: [],
      allSkills: [],
      allPersons: [],
      Filtered: [],
      skillPersonAll: [],
      is_admin: false,
      currentSkillPersons: [],
      currentPage: null,
      totalPages: null,
      nameFilter: '',
      skillFilter: '',
      levelFilter: undefined
    }

  }

  componentWillMount() {
    console.log(this.props.person.identity)
    var id_google = (this.props.person.identity)
    axios.get('/amal/api/Person/' + id_google)
      .then(response => {
        console.log("Admin :" + response.data.is_admin);
        this.setState({
          is_admin: response.data.is_admin,
        });

      })
      .catch(function (error) {
        console.log(error);

      })

  }
  componentDidMount() {
    axios.get('/amal/api/SkillPerson')
      .then(response => {
        console.log(response.data);
        this.setState({ allSkillPersons: response.data });
      })
      .catch(function (error) {
        console.log(error);
      })
    axios.get('/amal/api/Skill')
      .then(response => {
        console.log(response.data);
        this.setState({ allSkills: response.data });
      })
      .catch(function (error) {
        console.log(error);
      })

    axios.get('/amal/api/SkillPersonAll')
      .then(response => {
        console.log(response.data);
        this.setState({
          skillPersonAll: response.data
        });
      })
      .catch(function (error) {
        console.log(error);
      })


  }

  handleDataChange = ({ dataSize }) => {
    this.setState({ rowCount: dataSize });
  }

  optionFormatter(column, colIndex, { sortElement, filterElement }) {
    return (
      <div className="row">
        <div className="col-lg-12 ">
          <div className="active-cyan active-cyan-2">
            <div className="form-control">
              {filterElement}
              {column.text} <img src={ImgDesc} alt="imgDesc" width="10p%" height="10%" className="float"></img>
              {sortElement}
            </div>
          </div>
        </div>
      </div>
    );
  }



  render() {

    const selectOptions = {
      '1': '1',
      '2': '2',
      '3': '3',
      '4': '4'
    };

    const clearInfo = () => {
      this.setState.nameFilter('');
      this.setState.skillFilter('');
      this.setState.levelFilter('');
    };

    const columns = [
      {
        dataField: 'person.last_name',
        text: 'Consultant',
        sort: true,
        headerFormatter: this.optionFormatter,
        filter: textFilter({
          getFilter: (filter) => {
            this.setState.nameFilter = filter;
          }
        }),
        formatter: (cell, row, extraData) => (
          <div>
            <span><img src={row.person.img_url} alt="img" width="30" height="30" className="rounded-circle" /></span>&nbsp;&nbsp;<span>{row.person.last_name} {row.person.first_name}</span>
          </div>
        ),
      },

      {
        dataField: 'skill.name_skill',
        text: 'Skill',
        filter: textFilter({
          getFilter: (filter) => {
            this.setState.skillFilter = filter;
          }
        }),
        sort: true,
        headerFormatter: this.optionFormatter
      },
      {
        dataField: 'level',
        text: 'level',
        sort: true,

        filter: selectFilter({
          options: selectOptions,
          comparator: Comparator.LIKE,
          getFilter: (filter) => {
            this.setState.levelFilter = filter;
          }
        }),
        //headerStyle: {backgroundColor: '#5bc0de'},

        headerFormatter: this.optionFormatter,
        formatter: (cell, row, extraData) => (
          <div className="align-middle">
            <span>{row.level}</span>
            <Link to={"detail.skillPerson/" + row.id_person}><img src={readMoreImg} alt="readmore-img" width="20" height="20" className="button-border float-right" /></Link>
          </div>
        )
      },

    ];

    if (this.state.is_admin === true) {
      var isAdmin = (
        <div className="jumbotron  mt-1">
          <div className="row">
            <div className="col-lg-12">
              <li className="border-bottom border-gray text-search">Filter consultants data with at least one experience:</li>

            </div>
          </div>
          <br></br>
          <div className="row">
            <div className="col-lg-12">
              <Select
                isMulti
                /*onChange={entry => {
                  this.setState({ selectSkill: entry });
                  this.onFilteredChangeCustom(
                    entry.map(o => {
                      return o.value;
                    }),
                    "name_skill"
                  );
                }}*/
                value={this.state.selectSkill}
                options={this.state.allSkills.map((o, i) => {
                  return { id: i, value: o.name_skill, label: o.name_skill };
                })}
              />
            </div>
          </div>
          <br></br>
          <div className="row">
            <div className="col-lg-12">
              <Nav className="mr-auto">
                <div className="col-lg-6 align-middle text-left">
                  <Button className="btn btn-light " onClick={clearInfo}><span className="text-name-title">Clear all filters</span></Button>
                </div>
                <div className="col-lg-6 align-middle text-right">
                  <Link to="../create.Skill" className="btn btn-light " ><img src={addImg} alt="home-img" width="25" height="25" /> <span className="text-name-title">Add new skill </span></Link>
                </div>
              </Nav>
            </div>
          </div>
          <br></br><br></br>
          <div className="row">
            <div className="col-lg-12">
              <li className="border-bottom border-gray text-search">Filter consultants data with other options</li>

            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <BootstrapTable
                striped
                hover
                noDataIndication="Not data found"
                keyField='id'
                data={this.state.skillPersonAll}
                columns={columns}
                filter={filterFactory()}
                onDataSizeChange={this.handleDataChange}
                pagination={paginationFactory()}
                defaultSorted={this.defaultSorted} />
            </div>
          </div>
        </div>


        /*<div>
          <div className="jumbotron  mt-1">
            <div className="row">
              <div className="col-lg-12">
                <li className="border-bottom border-gray text-search">Filter consultants data with at least one experience:</li>

              </div>
            </div>
            <br></br>
            <div className="row">
              <div className="col-lg-12">
                <Select  
                  isMulti
                  onChange={entry => {
                    this.setState({ selectSkill: entry });
                    this.onFilteredChangeCustom(
                      entry.map(o => {
                        return o.value;
                      }),
                      "name_skill"
                    );
                  }}
                  value={this.state.selectSkill}
                  options={this.state.allSkills.map((o, i) => {
                    return { id: i, value: o.name_skill, label: o.name_skill };
                  })}
                />
              </div>
            </div>
            <br></br>
            <div className="row">
              <div className="col-lg-12">
                <li className="border-bottom border-gray text-search">Filter consultants data with other options</li>

              </div>
            </div>
            <br></br>

            <div className="row">
            <div className="col-lg-4 active-cyan active-cyan-2">
                                <input
                                    className="form-control"
                                    type="text"
                                    placeholder="Search with last name"
                                    icon="Last Name"
                                    onChange={this.onchangeLname}
                                />
                        </div>
                        <div className="col-lg-4 active-cyan active-cyan-2">
                                <input
                                    className="form-control"
                                    type="text"
                                    placeholder="Search first name"
                                    icon="First Name"
                                    onChange={this.onchangeFname}
                                />
                        </div>
           
            <div className="col-lg-4 active-cyan active-cyan-2">
              <input
                className="form-control"
                type="text"
                placeholder="Search with skill"
                icon="Skill name"
                onChange={this.onchangeSkill}
              />
            </div>
          </div>
        </div>
        <br></br>
        <h6 className="border-bottom border-gray pb-2 mb-0 text-name ">List of skills by consultant</h6>

        <div className="row d-flex flex-row py-4">

          <div className="w-100 px-4 py-2 d-flex flex-row flex-wrap align-items-center justify-content-between">
            <div className="d-flex flex-row align-items-center">

              <span className={headerClass}>
                <span className="text-secondary font-weight-bold">Total: </span><span className="text-secondary font-weight-bold">{totalSkillPersons}</span>
              </span>

              {currentPage && (
                <span className=" pl-4 text-secondary">
                  Page <span className="font-weight-bold">{currentPage}</span> / <span className="font-weight-bold">{totalPages}</span>
                </span>
              )}

            </div>
            <div className="d-flex flex-row  align-items-center">
              <Pagination totalRecords={totalSkillPersons} pageLimit={8} pageNeighbours={1} onPageChanged={this.onPageChanged} />
            </div>
          </div>

          <div className="border-table  person-list">
            {this.tabSkillPersons()}
          </div>
        </div>
        </div >*/

      )
    }

    else {
      var notAdmin = (
        < NotAdminPage />
      )
    }

    return (
      <>
        {isAdmin || notAdmin}
      </>
    )

  }
}
export default WithAuth(ListSkillPerson)
