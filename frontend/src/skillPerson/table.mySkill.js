import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import updateImg from '../pictures/update-img.png';
import deleleImg from '../pictures/delete-img.png';
import SweetAlert from 'react-bootstrap-sweetalert';

class TableMySkill extends Component {

    constructor(props) {
        super(props);
        //	this.onDeleteCustomer = this.onDeleteCustomer.bind(this);
        this.state = {
            alert: null

        };

    }



    componentDidMount() {
        axios.get('/amal/api/SkillPerson/' + this.props.my_skill.id_person)
       
        .catch(function (error) {
            console.log(error);
        })
}

    onDeleteMySkill() {

        axios.delete('/amal/api/SkillPerson/' + this.props.my_skill.id)
        this.hideAlert();
        this.props.history.push('/list.mySkill')
    }


    alertDelete() {

        const getAlert = () => (

            <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes, delete it!"
                confirmBtnBsStyle="danger"
                title="Are you sure!"
                onConfirm={() => this.onDeleteMySkill()}
                onCancel={() => this.hideAlert()}>
                You went to delete this skill of the list?
      </SweetAlert>
        );
        this.setState({
            alert: getAlert()

        });


    }

    hideAlert() {
        console.log('Hiding alert...');
        this.setState({
            alert: null
        });
    }


    render() {

        return (
            <tr>
                 <td className="d-flex align-items-center td-action">
                        <Link to={"edit.mySkill/" + this.props.my_skill.id } className="align-middle"><img src={updateImg} alt="update-img" width="18" height="18" className="button-border" /></Link>
                        <button onClick={() => this.alertDelete()} className="button-border">
                            <i className="fa fa-trash algin-middle" aria-hidden="true"></i><img src={deleleImg} alt="delete-img" width="20" height="20" />
                        </button>
                        {this.state.alert}
                </td>

                <td>
                    <div className="td-skill">
                        {this.props.my_skill.skill.name_skill}</div>
                </td>
                <td>
                    <div className="td-level">
                        {this.props.my_skill.level}
                    </div>
                </td>
                <td>
                    <div className="td-description">
                        {this.props.my_skill.my_skill_description}
                    </div>
                </td>
            </tr>



        );
    }
}

export default TableMySkill;
