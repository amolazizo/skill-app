import './index.css';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import { HashRouter, Route, withRouter} from 'react-router-dom';

//import PageNotFound from  './page.notFound';
import RegisterPerson from './person/register.person';
import ValidationPerson from './person/validation.person';
import UnregisteredPerson from './person/unregistered.person';
import ProfilePerson from './person/profile.person';
import ListPerson from './person/list.person';
import EditAdmin from './person/edit.admin';



import EmptyLevelTable from './level/empty.levelTable';
import CreateLevel from './level/create.level';
import Detaillevel from './level/detail.level';
import EditLevel from './level/edit.level';


import EmptyLinkSkillTable from './linkSkill/empty.linkSkillTable';
import CreateLinkSkill from './linkSkill/create.linkSkill';
import EditLinkSkill from './linkSkill/edit.linkSkill';

//import ListSkill from './skill/list.skill';
import CreateSkill from './skill/create.skill';
import EditSkill from './skill/edit.skill';
import EmptySkillTable from './skill/empty.skillTable';


import EmptySkillPersonTable from './skillPerson/empty.skillPersonTable';
import EmptyMySkillTable from './skillPerson/empty.mySkillTable';
import DetailSkillPerson from './skillPerson/detail.skillPerson';

import NotAdminPage from './not.admin.page';
import NavHome from './nav.home';
//import VerticalNavBarre from './vertical.nav.barre';
//import circleslogo from './pictures/circleslogo-white.png';

import CreateMySkill from './skillPerson/create.mySkill';
import EditMySkill from './skillPerson/edit.mySkill';


//import VerticalNavBarre from "./vertical_nav_barre";
//<div className="col-xl-4  text-white">
//<Route exact path='/' component={VerticalNavBarre} />
//</div>





class App extends Component {
  render() {
    return (
      <HashRouter>
        <div className="body-color">
          <div className="container">
              < NavHome />
            <div className="my-3 p-4 bg-white rounded">
              <div className="row">
                <Route exact path='/register.person' component={RegisterPerson} />
                <Route exact path='/'  component={ProfilePerson}/>
                <Route path='/empty.skillPersonTable' component={EmptySkillPersonTable} />
                <Route path='/edit.admin/:id' component={EditAdmin} />
                <Route path='/list.person' component={ListPerson} />

                <Route path='/create.skill' component={CreateSkill} />
                <Route path='/edit.skill/:id' component={EditSkill} />
                <Route path='/empty.skillTable' component={EmptySkillTable} />

                <Route path='/create.level' component={CreateLevel} />
                <Route path='/empty.levelTable' component={EmptyLevelTable} />
                <Route path='/detail.level/:id' component={Detaillevel} />
                <Route path='/edit.level/:id' component={EditLevel} />

                <Route path='/empty.mySkillTable' component={EmptyMySkillTable} />
                <Route path='/create.mySkill' component={CreateMySkill} />
                <Route path='/detail.skillPerson/:id_person' component={DetailSkillPerson} />
                <Route path='/edit.mySkill/:id' component={EditMySkill} />

                <Route path='/empty.linkSkillTable' component={EmptyLinkSkillTable} />
                <Route path='/create.linkSkill' component={CreateLinkSkill} />
                <Route path='/edit.linkSkill/:id' component={EditLinkSkill} />

                <Route path='/unregistered.person' component={UnregisteredPerson} />
                <Route path='/validation.person' component={ValidationPerson} />
                <Route path='/not.admin.page' component={NotAdminPage} />
              </div>
            </div>
          </div>
        </div>

      </HashRouter>

    );
  }

}

export default withRouter(App);
