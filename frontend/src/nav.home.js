import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import NavBarrePerson from './person/nav.barre.person';
//import { Nav } from 'react-bootstrap';
import circleslogo from './pictures/circleslogo-white.png';





class NavHome extends Component {

  render() {
    const regLink = (
      <div className="row d-flex align-items-center text-white-50 bg-info rounded shadow-sm">
      <div className="col-lg-1">
                <Link to="../regi">
                  <img src={circleslogo} alt="skill" width="80" height="80" /></Link>
              </div>
              <div className="col-lg-2">
                <h6 className="mb-0 text-white lh-100">Circles Skill</h6>
                <small className="text-white">Since 2019</small>
              </div>
              <div className="col-lg-9"></div>
             </div>
    )
    const navPerson = (

      <NavBarrePerson />

    )
    return (
      <>
        { localStorage.id_token ? navPerson : regLink }
      </>
    )
  }

}

export default withRouter(NavHome)
