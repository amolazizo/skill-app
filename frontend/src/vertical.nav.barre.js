import { Link, withRouter } from 'react-router-dom';
import homeWhite from './pictures/homeWhite.png';
//import profilepicture from './pictures/profile-img.png';
import yourProfileImg from './pictures/yourprofile-img.png';
import logoutImg from './pictures/logout.png';
import usersImg from './pictures/users-img.png';
import skillsImg from './pictures/skills-img.png';
import levelImg from './pictures/level-img.png';
import tecknologyImg from './pictures/tecknology-img.png';
import mySkillImg from './pictures/mySkill-img.png';





import React, { Component } from 'react';
import AuthService from './auth.service';
import axios from 'axios';
import WithAuth from './withAuth';
const Auth = new AuthService();


class VerticalNavBarre extends Component {
    constructor(props) {
        super(props);

        this.state = {
            is_admin: false,
            first_name: "",
            last_name: "",
            img_url:""
        }

    }

    componentDidMount() {
        console.log(this.props.person.identity)
        var id_google = this.props.person.identity;
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log(response);
                console.log('is_admin :' + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                    first_name: response.data.first_name,
                    last_name: response.data.last_name,
                    img_url: response.data.img_url,
                    
                });

            })
            .catch(function (error) {
                console.log(error);

            })
    }


    handleLogout()  {
        Auth.logout()
        this.props.history.replace('/register.person');

    }


    render() {

        if (this.state.is_admin === true) {
            var adminLinkBarreVertical = (
                <div className=" d-flex align-items-center row">
                <div className="col-xl top_margin">
                    <ul>
                        <li className="d-flex align-items-center">
                        <img src={this.state.img_url} className="rounded-circle" alt="home-img" width="50" height="50"/> &nbsp;&nbsp;
                             <p className="text-name-title  mb-0">{this.state.first_name} &nbsp;{this.state.last_name}</p>
                    </li>
                    </ul>
                    <br></br>
                    <hr></hr>
                </div>
                    <div className="sidebar btn col-xl">
                   
                        <ul>
                            <li className="d-flex align-items-center">
                                <img src={homeWhite} alt="home-img" width="25" height="25" />
                                <Link to="/empty.skillPersonTable"><h6 className="mb-0  title-menu-color">Home</h6></Link>
                            </li>
                            <li className="d-flex align-items-center">
                                <img src={usersImg} alt="users-img" width="25" height="25" />
                                <Link to="/list.person"><h6 className="mb-0  title-menu-color">Consultants</h6></Link>
                            </li>

                            <li className="d-flex align-items-center">
                                <img src={skillsImg} alt="skills-img" width="25" height="25" />
                                <Link to="/empty.skillTable"><h6 className="mb-0  title-menu-color">Skills</h6></Link>
                            </li>

                            <li className="d-flex align-items-center">
                                <img src={tecknologyImg} alt="teck-img" width="25" height="25" />
                                <Link to="/empty.linkSkillTable"><h6 className="mb-0  title-menu-color">Skill link</h6></Link>
                            </li>

                            <li className="d-flex align-items-center">
                                <img src={levelImg} alt="level-img" width="25" height="25" />
                                <Link to="/empty.levelTable"><h6 className="mb-0  title-menu-color">Levels</h6></Link>
                            </li>
                            <li className="d-flex align-items-center">   
                                <img src={mySkillImg} alt="mySkill-img" width="25" height="25" />
                                <Link to="/empty.mySkillTable"><h6 className="mb-0  title-menu-color">My skills</h6></Link>
                            </li>
                            <li className="d-flex align-items-center">
                                <img src={yourProfileImg} alt="myprofile-img" width="25" height="25" />
                                <Link to="/"><h6 className="mb-0  title-menu-color">My profile</h6></Link>
                            </li>
                        </ul>
                      
                        <div>
                    </div>
                    <br></br>
                    <hr></hr>
                    <br></br><br></br>
                    <div className="col-xl">
                            <h6 className="mb-0 title-menu-color">Logout</h6>
                            <Link to="/register.person" onClick={this.handleLogout.bind(this)}><img src={logoutImg} alt="logout-img" width="30" height="30" /></Link>
                        </div>
                        <br></br>
                        </div>
                    </div>
            )


        }
        else {
            var employeeLinkBarreVerstical = (
                <div className=" d-flex align-items-center row">
                <div className="col-xl top_margin">
                    <ul>
                        <li className="d-flex align-items-center">
                        <img src={this.state.img_url} className="rounded-circle" alt="home-img" width="50" height="50"/> &nbsp;&nbsp;
                             <p className="text-name-title  mb-0">{this.state.first_name} &nbsp;{this.state.last_name}</p>
                    </li>
                    </ul>
                    <br></br>
                    <hr></hr>
                </div>
                    <div className="sidebar btn col-xl">
                   
                        <ul>
                            <li className="d-flex align-items-center">
                                <img src={homeWhite} alt="home-img" width="25" height="25" />
                                <Link to="/"><h6 className="mb-0  title-menu-color">Home</h6></Link>
                            </li>
                            <li className="d-flex align-items-center">   
                                <img src={mySkillImg} alt="mySkill-img" width="25" height="25" />
                                <Link to="/empty.mySkillTable"><h6 className="mb-0  title-menu-color">My skills</h6></Link>
                            </li>

                            <li className="d-flex align-items-center">
                                <img src={skillsImg} alt="skills-img" width="25" height="25" />
                                <Link to="/create.skill"><h6 className="mb-0  title-menu-color">Add Skill</h6></Link>
                            </li>
                           
                        </ul>
                        <div>
                    </div>
                    <br></br>
                    <hr></hr>
                    <br></br><br></br>
                      <div className="col-xl">
                            <h6 className="mb-0 title-menu-color">Logout</h6>
                            <Link to="/" onClick={this.handleLogout.bind(this)}><img src={logoutImg} alt="logout-img" width="30" height="30" /></Link>
                        </div>
                        <br></br>
                        </div>
                    </div>
            )
        }
        return (

            <>

                {adminLinkBarreVertical || employeeLinkBarreVerstical}

            </>


        )
    }

}
export default withRouter(WithAuth(VerticalNavBarre));

