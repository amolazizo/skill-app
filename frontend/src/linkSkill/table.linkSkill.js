import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import updateImg from '../pictures/update-img.png';
import deleleImg from '../pictures/delete-img.png';
import SweetAlert from 'react-bootstrap-sweetalert';




class TableLinkSkill extends Component {

    constructor(props) {
        super(props);
        //	this.onDeleteCustomer = this.onDeleteCustomer.bind(this);
        this.state = {
            alert: null

        };

    }



    componentDidMount() {
        axios.get('/amal/api/LinkSkill/' + this.props.tab_linkSkill.id)
        .then(response => {
            console.log(response.data);
        })
            .catch(function (error) {
                console.log(error);
            })
    }


    onDeleteLinkSkill() {

        axios.delete('/amal/api/LinkSkill/' + this.props.tab_linkSkill.id)
        this.hideAlert();
        

    }


    alertDelete() {

        const getAlert = () => (

            <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes, delete it!"
                confirmBtnBsStyle="danger"
                title="Are you sure!"
                onConfirm={() => this.onDeleteLinkSkill()}
                onCancel={() => this.hideAlert()}>
                You will not be able to recover this skill
      </SweetAlert>
        );

        this.setState({
            alert: getAlert()

        });


    }

    hideAlert() {
        console.log('Hiding alert...');
        this.setState({
            alert: null
        });
    }

    render() {
        return (
     <tr>
                
             <td>
                    {this.props.tab_linkSkill.link_upper.name_skill}
                </td>
                <td>
                    {this.props.tab_linkSkill.link_lower.name_skill}
                </td>
                <td className="d-flex align-items-center">


	                <Link to={"edit.linkSkill/"+this.props.tab_linkSkill.id} className="align-middle"><img src={updateImg} alt="update-img" width="20" height="20" className="button-border" /></Link>
                    <button onClick={() => this.alertDelete()}  className="button-border">
                     <i className="fa fa-trash algin-middle" aria-hidden="true"></i><img src={deleleImg} alt="delete-img" width="23" height="23" />
	                 </button>
                     {this.state.alert}
        
        </td>

                
            </tr>



        );
    }
}

export default TableLinkSkill;
