
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import NotAdminPage from '../not.admin.page';
import WithAuth from '../withAuth';
import ListLinkSkill from './list.linkSkill';
import VerticalNavBarre from '../vertical.nav.barre';


class EmptyLinkSkillTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
            links: []

        }

    }

    componentWillMount() {
        console.log(this.props.person.identity)
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("Admin :" + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                });

            })
            .catch(function (error) {
                console.log(error);

            })

    }

    componentDidMount() {
        axios.get('/amal/api/LinkSkill')
            .then(response => {
                console.log(response.data);
                this.setState({ links: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    isAdmin() {

        const totalLinks = this.state.links.length;
        if (this.state.is_admin === true) {
            if (totalLinks === 0) {
                return (
                    <>
                        <div className="jumbotron mt-5">
                            <div className="col-lg-12 mx-auto">
                                <div>
                                    <h3>No data for link of skill!</h3>
                                    <br></br>
                                    <p>Please insert data in database.</p>
                                </div>

                            </div>
                        </div>
                        <div>
                            <footer>
                                <nav className="navbar navbar-expand-lg navbar-dark bg-info rounded">
                                    <div
                                        className="collapse navbar-collapse justify-content-md-center"
                                        id="navbarsExample10"
                                    >
                                        <ul className="navbar-nav">
                                            <li className="nav-item">
                                                <Link to="/create.linkSkill" className="btn mb-0 text-white"><h6> Add new link</h6>
                                                   
                                                 </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </footer>
                        </div>
                    </>
                )
            }
            else {
                return (

                    <ListLinkSkill />

                )

            }

        }

        else {
            return (
                < NotAdminPage />
            )
        }
    }

    render() {

        return (
            <>
            <div className="col-lg-3 bg-info rounded">
            <VerticalNavBarre />
            </div>
            <div className="col-lg-9 pl-4">
                {this.isAdmin()}
            </div>
            </>
        )


    }
}

export default WithAuth(EmptyLinkSkillTable)
