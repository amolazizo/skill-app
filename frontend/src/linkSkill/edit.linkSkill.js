import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { Link } from 'react-router-dom';



class EditLinkSkill extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedLinkSkillId:1,
            selectedSkillId:1,
            name_skill:"",
            name_linkSkill:"",
            skills: []
        }
    }

    componentWillMount() {

        axios.get("/amal/api/Skill")
            .then(response => {
                console.log(response.data);
                this.setState({ skills: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });

        }

        componentDidMount() {
        axios.get('/amal/api/LinkSkill/' + this.props.match.params.id)
            .then(response => {
                console.log(response.data.link_lower.name_skill)
                console.log(response.data.link_upper.name_skill)
                this.setState({
            
                    id_link_upper: response.data.selectedLinkSkillId,
                    id_link_lower: response.data.selectedSkillId,
                    name_linkSkill: response.data.link_upper.name_skill,
                    name_skill: response.data.link_lower.name_skill,


                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }


    onChangeLinkSkillName = (e) => {
        console.log("got event value: " + JSON.stringify(e.target.value))
        e.preventDefault()
        this.setState({
            selectedLinkSkillId: e.target.value
        });
    }


    onChangeSkillName = (e) => {
        console.log("got event value: " + JSON.stringify(e.target.value))
        e.preventDefault()
        this.setState({
            selectedSkillId: e.target.value
        });
    }


    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,
        });
    };

    editLinkSkill() {

        const link = {
            id_link_upper: this.state.selectedLinkSkillId,
            id_link_lower: this.state.selectedSkillId

        };

        const url = `${API_URL}/LinkSkill/` + this.props.match.params.id;

        axios.put(url, link)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.linkSkillTable');

            }).catch((err) => {
                console.log(err);
            });
    }

    handleEditLinkSkill = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: ' You want to save this change?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.editLinkSkill(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };

    render() {

        const linkSkillOptions = this.state.skills.map(function (skill) {
            return (
                <option key={skill.id} value={skill.id}>{skill.name_skill}</option>
            );
        });

        const skillOptions = this.state.skills.map(function (skill) {
            return (
                <option key={skill.id} value={skill.id}>{skill.name_skill}</option>
            );
        });


        return (

            <div className="jumbotron  mt-1">
                <h6 className="border-bottom border-gray">Make changes to the link of skill:</h6>
                <div className="pt-3"></div>
                <form onSubmit={this.handleEditLinkSkill}>

                    <div className="row skill-margin">
                        <div className="col-xl-4 text-muted"><strong className="text-gray-dark">Link of skill:</strong>
                        </div>

                        <div className="small col-xl-7">
                            <select
                                onChange={this.onChangeLinkSkillName}
                                className='form-control'
                                required="required"
                                value={this.state.selectedLinkSkillId}>
                                <option value="1">{this.state.name_linkSkill}</option>
                                {linkSkillOptions}
                            </select>

                        </div>
                        <div className="col-xl-1"></div>
                    </div>

                    <div className="row skill-margin">
                        <div className="col-xl-4 text-muted"><strong className="text-gray-dark">Skill name:</strong>
                        </div>

                        <div className="small col-xl-7">
                            <select
                                onChange={this.onChangeSkillName}
                                className='form-control'
                                required="required"
                                value={this.state.selectedSkillId}>
                                <option value="1">{this.state.name_skill}</option>
                                {skillOptions}
                            </select>

                        </div>
                        <div className="col-xl-1"></div>
                    </div>


                    <div className="row skill-margin">
                        <div className="col-xl-11 float-righ">
                            <div className="float-right px-2">
                                <button
                                    type="submit"
                                    className="btn btn-info"
                                    required="required"
                                >Update</button>
                            </div>
                            <div className="float-right px-2">
                                <Link to="/Empty.linkSkillTable" className="btn btn-info">
                                    Return
                                </Link>
                            </div>
                        </div>
                        <div className="col-xl-1"></div>

                    </div>



                </form >
            </div >


        )
    }

}




export default EditLinkSkill