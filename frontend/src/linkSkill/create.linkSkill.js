import React, { Component } from 'react';
import axios from 'axios';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import  { Link } from 'react-router-dom';

class CreateLinkSkill extends Component {

    constructor(props) {
        super(props);

        this.state = {
            skills: [],
            selectedSkillId: -1,
            selectLinkSkillId: -1
            

        };
    }

    componentDidMount() {
        axios.get("/amal/api/Skill")
            .then(response => {
                console.log(response.data);
                this.setState({ skills: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,

        });
    };

    CreateLinkSkill() {

        const linkSkill = {
            id_link_lower: this.state.selectedSkillId,
            id_link_upper: this.state.selectedLinkSkillId
        };

        const url = `${API_URL}/LinkSkill`;

        axios.post(url, linkSkill)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.linkSkillTable');


            }).catch((err) => {
                console.log(err);
            });
    }


    handleLinkSkillInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
             [name]: value });
    }

   
    handleCreateLinkSkill = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: 'You are sure ?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.CreateLinkSkill(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };


    render() {

        const linkSkillOptions = this.state.skills.map(function (skill) {
            return (
                <option key={skill.id} value={skill.id}>{skill.name_skill}</option>
            );
        });

        const skillOptions = this.state.skills.map(function (skill) {
            return (
                <option key={skill.id} value={skill.id}>{skill.name_skill}</option>
            );
        });

     

        return (
            <div className="jumbotron ">
                <h6 className="border-bottom border-gray">Add new link for skill</h6>
                <div className="pt-3"></div>
                <form onSubmit={this.handleCreateLinkSkill}>
                    <table className='table table-borderless'>
                        <tbody>

                        <tr>
                                <td className="text-muted"><strong className="text-gray-dark">Link for skill:</strong></td>
                                <td>
                                    <select
                                        onChange={(event) => this.handleLinkSkillInput(event)}
                                        className='form-control'
                                        name="selectedLinkSkillId"
                                        value={this.state.selectedLinkSkillId}>
                                        <option value="-1">Select link...</option>
                                        {linkSkillOptions}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td className="text-muted"><strong className="text-gray-dark">Skill name:</strong></td>
                                <td>
                                    <select
                                        onChange={(event) => this.handleLinkSkillInput(event)}
                                        className='form-control'
                                        name="selectedSkillId"
                                        value={this.state.selectedSkillId}>
                                        <option value="-1">Select skill...</option>
                                        {skillOptions}
                                    </select>
                                </td>

                            </tr>
                            <br></br><br></br>
                            <tr>
                                <td></td>
                                <td className="float-right">
                                    <button
                                        type="submit"
                                        className="btn btn-info">Add link</button>
                                </td>
                                <td className="float-right px-2">
                                <Link to="/empty.linkSkillTable" className="btn btn-info">
                                        Return
                                </Link>
                                </td> 
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>

        );
    }


}
export default CreateLinkSkill;

