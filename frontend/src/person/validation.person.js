import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class ValidationPerson extends Component {


  render() {

    return (

      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
            <div>
              <h1>Welcome!</h1><br>
              </br>
              <p>You have registered!
                    Please click <Link to="/login.person"> here </Link>to login.
                </p>
            </div>

          </div>
        </div>
      </div>

    )
  }
}



