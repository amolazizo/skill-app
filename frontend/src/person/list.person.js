import React, { Component } from 'react';
import axios from 'axios';
import WithAuth from '../withAuth';
import NotAdminPage from '../not.admin.page';
import searchImg from '../pictures/search.png';
import { Link } from 'react-router-dom';
import updateImg from '../pictures/update-admin.png';
import VerticalNavBarre from '../vertical.nav.barre';




import Pagination from '../pagination';



class ListPerson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
            allPersons: [],
            currentPersons: [],
            currentPage: null,
            totalPages: null,
            searchPerson: ""
        }
    }

    componentWillMount() {
        console.log(this.props.person.identity)
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("Admin :" + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                });

            })
            .catch(function (error) {
                console.log(error);

            })

    }
    componentDidMount() {
        axios.get('/amal/api/Person')
            .then(response => {
                console.log(response.data);
                this.setState({ 
                    allPersons: response.data  });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onPageChanged = data => {
        const { allPersons } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentPersons = allPersons.slice(offset, offset + pageLimit);

        this.setState({ currentPage, currentPersons, totalPages });
    };

    onchange = e => {
        this.setState({
            searchPerson: e.target.value
        });
    };

    isAdminPerson(person) {
            console.log("person is admin :"+ person.is_admin);
                    if ( person.is_admin === true) {
                        return "Admin";
                         }
                    else {
                         return "";
                             }
                }
                        
    renderPersons = person => {
        return (
            <>
                <tr>
                    <td>
                        <img src={person.img_url} alt="home-img" width="30" height="30" className="rounded-circle" />&nbsp;&nbsp;&nbsp;&nbsp;
               </td>
                    <td>
                        {person.last_name.substring(0, 15)} {person.last_name.length > 15 && "..."}   {person.first_name.substring(0, 15)} {person.first_name.length > 15 && "..."}
                        <span className="admin-title">{this.isAdminPerson(person)}
                        </span>
                        <Link to={"edit.admin/" + person.id} className="admin-text text-sign-in">
                            <img src={updateImg} alt="update-img" width="20" height="20" className="button-border" /></Link>
                    </td>
                    <td>
                        {person.email_person}
                    </td>
                </tr>
            </>
        );
    };

    render() {

        const {
            allPersons,
            currentPage,
            totalPages
        } = this.state;
        const totalPersons = allPersons.length;
        const { searchPerson } = this.state;

        const filteredPersons = allPersons.filter(person => {
            return (
                (person.last_name.toLowerCase().indexOf(searchPerson.toLowerCase()) !== -1) || (person.first_name.toLowerCase().indexOf(searchPerson.toLowerCase()) !== -1)
            );
        });


        const filterPerson = filteredPersons.map(person => {
            return this.renderPersons(person);
        });

        if (totalPersons === 0) return null;

        const headerClass = [
            "text-dark py-2 pr-4 m-0",
            currentPage ? "border-gray border-right" : ""
        ]
            .join(" ")
            .trim();

        if (this.state.is_admin === true) {
            var isAdmin = (
                <div>
                    <div className="jumbotron  mt-1">
                    <div className="row">
                        <div className="col-lg-12">
                           <li className="border-bottom border-gray text-search">Look for a consultant by his last name or first name:</li>
                           
                        </div>
                    </div>
                    <br></br>

                    <div className="row">
                        <div className="col-lg-1 search-icon"><img src={searchImg} alt="home-img" width="30" height="30" /></div>
                        <div className="col-lg-11 active-cyan active-cyan-2">
                            <input
                                className="form-control"
                                type="text"
                                placeholder="Search Consultant"
                                icon="search"
                                onChange={this.onchange}
                            />
                        </div>
                    </div>
                    </div>
                    <h6 className="border-bottom border-gray text-name ">List of consultants</h6>
                    <div className="row d-flex flex-row py-4">

                        <div className="w-100 px-4 py-2 d-flex  align-items-center justify-content-between">
                            <div className="d-flex flex-row align-items-center">

                                <span className={headerClass}>
                                    <span className="text-secondary font-weight-bold">Total: </span><span className="text-secondary font-weight-bold">{totalPersons}</span> Consultants
                    </span>

                                {currentPage && (
                                    <span className="pl-4 text-secondary">
                                        Page <span className="font-weight-bold">{currentPage}</span> / <span className="font-weight-bold">{totalPages}</span>
                                    </span>
                                )}

                            </div>
                            <div className="d-flex flex-row  align-items-center">
                                <Pagination totalRecords={totalPersons} pageLimit={6} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                            </div>
                        </div>

                        <div className="border-table person-list">

                            <table className="table table-hover">
                                <thead className="thead-light">
                                    <tr>
                                        <th></th>
                                        <th className="person-list-person">Consultant</th>
                                        <th className="list-skill-person">Email</th>
                                    </tr>
                                </thead>
                                <tbody className="">
                                    {filterPerson}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            );

        }



        else {
            var notAdmin = (
                < NotAdminPage />
            )
        }

        return (

            <div className="row">
            <div className="col-lg-3 bg-info rounded">
            <VerticalNavBarre />
            </div>
            <div className="col-lg-9 pl-4">

                {isAdmin || notAdmin}
           </div>
           </div>
           
        )



    }

}
export default WithAuth(ListPerson)