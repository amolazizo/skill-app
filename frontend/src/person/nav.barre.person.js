import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import AuthService from '../auth.service';
//import { Navbar} from 'react-bootstrap';
import axios from 'axios';
import WithAuth from '../withAuth';
import circleslogo from '../pictures/circleslogo-white.png';


const Auth = new AuthService();



class NavBarrePerson extends Component {
  constructor(props) {
    super(props);

    this.state = {
      is_admin: false,
      email_person: ""
    }

  }

  componentDidMount() {
    console.log(this.props.person.identity)
    var id_google = this.props.person.identity;
    axios.get('/amal/api/Person/' + id_google)
      .then(response => {
        console.log(response);
        console.log('is_admin :' + response.data.is_admin);
        this.setState({
          email_person: response.data.email_person,
          is_admin: response.data.is_admin
        });

      })
      .catch(function (error) {
        console.log(error);

      })
  }


  handleLogout() {
    Auth.logout()
    this.props.history.replace('/register.person');
  }

  render() {

    return (
      <div className="row d-flex align-items-center text-white-50 bg-info rounded shadow-sm">
              <div className="col-lg-1">
                <Link to="../register.person">
                  <img className="mr-3" src={circleslogo} alt="skill" width="80" height="80" /></Link>
              </div>
              <div className="col-lg-2">
                <h6 className="mb-0 text-white lh-100">Circles Skill</h6>
                <small className="text-white">Since 2019</small>
              </div>
              <div className="col-lg-9">
              <p className="mb-0 text-white lh-100 text-right"><span className="text-white-50">Signed in as :</span> &nbsp;{this.state.email_person}</p>
              </div>
             </div>
    )

}
}
export default withRouter(WithAuth(NavBarrePerson));
