import React, { Component } from 'react';
import { GoogleLogin } from 'react-google-login';
import AuthService from '../auth.service';
import axios from 'axios';
import authuser from '../pictures/auth-user.png';




export default class LoginPerson extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id_google: "",
        };
        this.onLoginSuccess = this.onLoginSuccess.bind(this);
        this.Auth = new AuthService();
    }

     onLoginSuccess = (event) => {
        const profileObj = event.profileObj;
        console.log(profileObj)
        axios.get('/amal/api/Person/' + profileObj.googleId)
        .then(res => {
            try {
            if (res.status === 200 && res.data.id_google === profileObj.googleId) {
                this.Auth.login(profileObj.googleId);
                return (
                    this.props.history.replace('/login.person')
                )

            }
            else {
                this.props.history.replace('../unregistered.person')
            }
            
        }
        catch(error){
            console.log(error)
            }
        })

    }



    async componentDidMount() {

        if (this.Auth.loggedIn()) {
            return (
                this.props.history.replace('/empty.skillPersonTable')


            )
        }
    }


    render() {
        return (

            <div className="align-items-center login-div">
                <h6 className="border-gray pb-2 mb-0 admin-title">Sign in with your google account for circles.fi:</h6>
                <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                <div className="d-flex justify-content-center margin-left">
                    <img src={authuser} alt="skill" width="80" height="80" />
                </div>
                <div className="pt-3 d-flex justify-content-center">

                    <GoogleLogin
                        clientId="149811966527-oitrpktl0e86q4pbn42b7crts59v7qqp.apps.googleusercontent.com"
                        render={renderProps => (
                            <button onClick={renderProps.onClick} disabled={renderProps.disabled} className="google-btn-login bg-info">Login</button>
                        )}
                        onSuccess={this.onLoginSuccess}
                        onFailure={this.onLoginFailure} />
                </div>
            </div>
        );
    }

}

