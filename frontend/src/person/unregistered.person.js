import React, {Component} from 'react';
import { Link }  from 'react-router-dom';

class UnregisteredPerson extends Component {


render() {


return (

    <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
          <div>
                 <h1>Hello!</h1><br>
                </br>
                <p>You are not registered yet -
                   Please signup to login  to your account.
                </p>
          </div>
        
          </div>
        </div>
      <div>
       <footer>
       <nav className="navbar navbar-expand-lg navbar-dark bg-info rounded">
     <div
       className="collapse navbar-collapse justify-content-md-center"
       id="navbarsExample10"
     >
       <ul className="navbar-nav">
     <li className="nav-item">
       <Link to="/register.person" className="nav-link text-white">
         Register now
       </Link>
     </li>
     </ul>
     </div>
     </nav>
     </footer>  
     </div>
     </div>


)
}
}
export default UnregisteredPerson;

