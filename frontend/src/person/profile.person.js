import React, { Component } from 'react';
import WithAuth from '../withAuth';
import axios from 'axios';
import { Link, withRouter } from 'react-router-dom';
//import { Nav, Navbar, Button, Table} from 'react-bootstrap';
//import { Button } from 'react-bootstrap';
import VerticalNavBarre from '../vertical.nav.barre';


class ProfilePerson extends Component {

  constructor(props) {
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      email_employee: '',
      img_url: '',
      is_admin: false
    }
  }
  componentDidMount() {
    console.log(this.props.person.identity)
    var id_google = this.props.person.identity;
    axios.get('/amal/api/Person/' + id_google)
      .then(response => {
        console.log(response);
        this.setState({
          first_name: response.data.first_name,
          last_name: response.data.last_name,
          email_person: response.data.email_person,
          is_admin: response.data.is_admin,
          img_url: response.data.img_url

        });

      })
      .catch(function (error) {
        console.log(error);

      })

  }

  isAdminPerson() {
    if (this.state.is_admin === true) {
      return "Admin";
    }
    else {
      return "Not administator";
    }
  }

  render() {
    return (
      <>
        <div className="col-lg-3 bg-info rounded">
          <VerticalNavBarre />
        </div>
        <div className="col-lg-9 pl-4">
          <div className="jumbotron  mt-1">
            <h6 className="text-name">My profile:</h6>
            <div className="pt-3"></div>
            <table className='table table-borderless bg-light rounded'>
              <tbody className="">
                <tr>
                  <td>
                    <img src={this.state.img_url} alt="home-img" width="100" height="100" className="rounded" />&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>
                </tr>
                <tr>
                  <td className=" border-bottom border-gray"><h6>Full Name:</h6></td>
                </tr>
                <tr>
                  <td>
                    {this.state.first_name} {this.state.last_name}
                  </td>
                </tr>
                <tr>
                  <td className="border-bottom border-gray"><h6>Address email:</h6></td>
                </tr>
                <tr>
                  <td >
                    {this.state.email_person}
                  </td>
                </tr>
                <tr>
                  <td className=" border-bottom border-gray"><h6>Status:</h6></td>
                </tr>
                <tr>
                  <td>
                    <span className="admin-title">{this.isAdminPerson()}</span>
                  </td>
                </tr>

              </tbody>
            </table>
            <div className="mt-5">
              <Link to="../empty.mySkillTable" className="btn bg-info"><h6 className="mb-0  title-menu-color">Return</h6></Link>
            </div>
          </div>
        </div>

      </>)
  }
}


/*
   <Navbar bg="light" variant="dark" >
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
          <Button href="/list.hours" variant="outline-secondary">Add hours</Button>
            
          </Nav>
         
        </Navbar.Collapse>
      </Navbar>
      */
export default withRouter(WithAuth(ProfilePerson))
