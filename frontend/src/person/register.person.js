import React, { Component } from 'react';
import { GoogleLogin } from 'react-google-login';
import axios from 'axios';
import authuser from '../pictures/auth-user.png';
import AuthService from '../auth.service';
import { withRouter} from 'react-router-dom';




//import config from '../Config';


class RegisterPerson extends Component {

    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
        }
        this.Auth = new AuthService();
    }
    async getPerson(googleId) {
        try {
            var res = await axios.get('/amal/api/Person/' + googleId)
            return res.status
        }
        catch (error) {
            console.log(error)
        }
    }




    onLoginSuccess = (event) => {
        const profileObj = event.profileObj;
        console.log(profileObj)
        if (this.getPerson(profileObj.googleId) === 200) {
            sessionStorage.setState("googleProfile", profileObj)
        }
        // add new employee
        axios.post('/amal/api/Person',
            {
                "first_name": profileObj.givenName,
                "last_name": profileObj.familyName,
                "email_person": profileObj.email,
                "is_admin": false,
                "id_google": profileObj.googleId,
                "img_url": profileObj.imageUrl,

            })
            .then(res => {
                try {
                    if (res.status === 200) {
                        this.Auth.login(profileObj.googleId);
                        return (
                            this.props.history.replace('/')

                        )

                    }

                }
                catch (error) {
                    console.log(error)
                }
            })
    }

    async componentDidMount() {
        if (this.Auth.loggedIn()) {
            return (
                this.props.history.replace('/')
            )

    }
}


    render() {
        return (
            <div className="col-lg-12">
                                <div className="align-items-center login-div">
                        <h6 className="border-gray pb-2 mb-0 admin-title">Use your google account for circles.fi to login:</h6>
                        <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                        <div className="d-flex justify-content-center margin-left">
                            <img src={authuser} alt="skill" width="80" height="80" />
                        </div>
                        <div className="pt-2 d-flex justify-content-center">
                            <GoogleLogin
                                clientId="149811966527-oitrpktl0e86q4pbn42b7crts59v7qqp.apps.googleusercontent.com"
                                render={renderProps => (
                                    <button onClick={renderProps.onClick} disabled={renderProps.disabled} className="google-btn-register text-info">Login with Google account</button>
                                )}
                                onSuccess={this.onLoginSuccess.bind(this)}
                                onFailure={this.onLoginFailure} />
                        </div>

                    </div>
                    </div>
        );
    }
}

export default withRouter(RegisterPerson)