import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';

class EditAdmin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
            first_name: '',
            last_name:'',
            email_person:"",
            id_google:"",
            img_url:""
        }
    }

    componentDidMount() {
        console.log("look for ID consultant:",this.props.match.params.id);
        axios.get('/amal/api/PersonId/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    first_name: response.data.first_name,
                    last_name: response.data.last_name,
                    email_person: response.data.email_person,
                    is_admin: response.data.is_admin,
                    id_google: response.data.id_google, 
                    img_url: response.data.img_url
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }


    onChangeIsAdminPerson = (e) => {
            e.preventDefault()
            console.log(e.target.value)
            this.setState({
                is_admin: e.target.value
            });
        }


    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,
        });
    };


    editPerson() {
        const person = {
            first_name: this.state.first_name,
            last_name:this.state.last_name,
            email_person: this.state.email_person,
            is_admin: Boolean(this.state.is_admin), 
            id_google: this.state.id_google,
            img_url: this.state.img_url

        };

        const url = `${API_URL}/Person/`+ this.props.match.params.id;

        axios.put(url, person)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/list.person');

            }).catch((err) => {
                console.log(err);
            });
    }
   
    handleEditIsAdminPerson = (e) => {
        e.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: ' You want to save this change?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.editPerson(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };

    isAdminPerson() {
        if (this.state.is_admin === true) 
        {
            return "is administartor ";
        }
        else
        {
            return "is not administrator";
        }
    }


    render() {

        const normalizeBoolean = (checked) => {
            if (checked=== true) {
                return true;
            }
        
            if (checked === false) {
              return false;
            }
        
            return checked;
           
        };
    
        return (

            <div>
                <h6 className="border-bottom border-gray pb-2 mb-0 skill-margin">Change the Admin status for this consultant:</h6>
                <div className="pt-3"></div>
                <div className="text-muted">
                <img src={this.state.img_url} alt="home-img" width="100" height="100" className="rounded" />&nbsp;&nbsp;&nbsp;&nbsp;
                  </div>
                  <div className="pt-3"></div>
                  <div>
                <p className="text-name  mb-0">{this.state.first_name} {this.state.last_name}</p>
                <div className="pt-3"></div>
                 <span className="admin-title"><b>{this.isAdminPerson()} </b></span>
                </div>
                
                <form onSubmit={this.handleEditIsAdminPerson}>
                    <div className="row skill-margin ">
                        <div className="col-lg-6 text-muted"><strong className="text-gray-dark">Admin status:</strong>
                        </div>

                        <div className="small col-lg-6">
                        <div className="row">
                                <div className="col-lg-6">
                                <input
                                    type="radio" 
                                    name="admin"
                                    value={true}
                                    checked={normalizeBoolean}
                                    onChange={this.onChangeIsAdminPerson}
                                    /> &nbsp;&nbsp; 
                                <label for="Administator">Administator</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-6">
                                <input
                                    type="radio"
                                    name="admin" 
                                    value={false}
                                    checked={normalizeBoolean}
                                    onChange={this.onChangeIsAdminPerson}
                                    />&nbsp;&nbsp;
                                <label for="Administator">Not administator</label>
                                </div>
                            </div>
                            
                        </div>
                        </div>
                       
                        <div className="row skill-margin">
                        <div className="col-xl-11 float-righ">
                            <div className="float-right">
                            <button 
                            type="submit" 
                            className="btn btn-info"
                            required="required"
                            >Update status</button>
                            </div>
                        </div>
                        <div className="col-xl-1"></div>

                    </div>
                </form >
            </div >

         
      )
    }

}




export default EditAdmin