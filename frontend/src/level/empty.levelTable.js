
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import NotAdminPage from '../not.admin.page';
import WithAuth from '../withAuth';
import ListLevel from './list.level';
import VerticalNavBarre from '../vertical.nav.barre';


class EmptyLevelTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
            levels: []

        }

    }

    componentWillMount() {
        console.log(this.props.person.identity)
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("Admin :" + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                });

            })
            .catch(function (error) {
                console.log(error);

            })

    }

    componentDidMount() {
        axios.get('/amal/api/DescriptionLevel')
            .then(response => {
                console.log(response.data);
                this.setState({ levels: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    isAdmin() {

        const totalLevels = this.state.levels.length;
        if (this.state.is_admin === true) {
            if (totalLevels === 0) {
                return (
                    <div className="container">
                        <div className="jumbotron mt-5">
                            <div className="col-xl-8 mx-auto">
                                <div>
                                    <h3>No data for description of level!</h3>
                                    <br></br>
                                    <p>Please insert data in database.</p>
                                </div>

                            </div>
                        </div>
                        <div>
                            <footer>
                                <nav className="navbar navbar-expand-lg navbar-dark bg-info rounded">
                                    <div
                                        className="collapse navbar-collapse justify-content-md-center"
                                        id="navbarsExample10"
                                    >
                                        <ul className="navbar-nav">
                                            <li className="nav-item">
                                                <Link to="/create.level" className="btn mb-0 text-white"><h6> Add new level</h6>
                                                   
                                                 </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </footer>
                        </div>
                    </div>
                )
            }
            else {
                return (

                    <ListLevel />

                )

            }

        }

        else {
            return (
                < NotAdminPage />
            )
        }
    }

    render() {

        return (
            <div className="row">
            <div className="col-lg-3 bg-info rounded">
            <VerticalNavBarre />
            </div>
            <div className="col-lg-9 pl-4">
                {this.isAdmin()}
           </div>
           </div>
        )


    }
}

export default WithAuth(EmptyLevelTable)
