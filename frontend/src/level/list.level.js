import React, { Component } from 'react';
import axios from 'axios';
import WithAuth from '../withAuth';
import { Nav, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import NotAdminPage from '../not.admin.page';
import searchImg from '../pictures/search.png';
import addImg from '../pictures/add-img-grey.png';
import updateImg from '../pictures/update-img-grey.png';
import deleleImg from '../pictures/delete-img-grey.png';
import SweetAlert from 'react-bootstrap-sweetalert';
import readMoreImg from '../pictures/read-more-grey.png'




import Pagination from '../pagination.js';

//import { Link } from 'react-router-dom';

class ListLevel extends Component {



    constructor(props) {
        super(props);
        this.state = {
            is_admin: false,
            allLevels: [],
            currentLevels: [],
            currentPage: null,
            totalPages: null,
            searchLevel: 0,
            searchSkill: "",
            alert: null

        }

    }

    componentWillMount() {
        console.log(this.props.person.identity)
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("Admin :" + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                });

            })
            .catch(function (error) {
                console.log(error);

            })

    }




    componentDidMount() {
        axios.get('/amal/api/DescriptionLevel')
            .then(response => {
                console.log(response.data);
                this.setState({ allLevels: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onPageChanged = data => {
        const { allLevels } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentLevels = allLevels.slice(offset, offset + pageLimit);

        this.setState({ currentPage, currentLevels, totalPages });
    };

    /*onchangeLevel = e => {
        this.setState({
            searchLevel: e.target.value
        });
    };*/

    onChangeSkill = e => {
        this.setState({
            searchSkill: e.target.value
        });
    }



    hideAlert() {
        console.log('Hiding alert...');
        this.setState({
            alert: null
        });
    }



    renderLevels = level => {
        return (
            <>
                <tr>
                    <td className="d-flex align-items-center">
                <Link to={"edit.level/" + level.id} className="align-middle"><img src={updateImg} alt="update-img" width="18" height="18" className="button-border" /></Link>

                        <button onClick={() => {
                            const getAlert = () => (
                                <SweetAlert
                                    warning
                                    showCancel
                                    confirmBtnText="Yes, delete it!"
                                    confirmBtnBsStyle="danger"
                                    title="Are you sure!"
                                    onConfirm={() => {
                                        axios.delete('/amal/api/DescriptionLevel/' + level.id)
                                        this.hideAlert();

                                    }
                                    }
                                    onCancel={() => this.hideAlert()}>
                                    You will not be able to recover this skill
                                                </SweetAlert>
                            );

                            this.setState({
                                alert: getAlert()

                            });
                        }

                        } className="button-border">
                            <i className="fa fa-trash algin-middle" aria-hidden="true"></i><img src={deleleImg} alt="delete-img" width="20" height="20" />
                        </button>
                        <Link to={"detail.level/" + level.id} className="align-middle"><img src={readMoreImg} alt="readmore-img" width="20" height="20" className="button-border" /></Link>&nbsp;&nbsp;
                        {this.state.alert}

                    </td>

                    <td>
                        {level.skill.name_skill.substring(0, 15)} {level.skill.name_skill.length > 15 && "..."}
                    </td>
                    <td>
                        {level.level_number}
                    </td>
                    <td>
                        {level.level_description}
                    </td>
                </tr>

            </>
        );
    };

    render() {

        const {
            allLevels,
            currentLevels,
            currentPage,
            totalPages
        } = this.state;
        const totalLevels = allLevels.length;


        if (totalLevels === 0) return null;

        const headerClass = [
            "text-dark py-2 pr-4 m-0",
            currentPage ? "border-gray border-right" : ""
        ]
            .join(" ")
            .trim();

       //const { searchLevel } = this.state;
        const { searchSkill } = this.state;

        const filteredLevels = currentLevels.filter(level => {
            return ( level.skill.name_skill.toLowerCase().indexOf(searchSkill.toLowerCase()) !== -1)


        });


        const filterDescriptionLevel = filteredLevels.map(level => {
            return this.renderLevels(level);
        });

        if (this.state.is_admin === true) {
            var isAdmin = (
                <div>

                    <div className="jumbotron  mt-1">
                        <div className="row">
                            <div className="col-lg-4">
                                <Nav className="mr-auto">
                                    <Button href="/create.level" className="btn btn-light " ><img src={addImg} alt="home-img" width="25" height="25" /><span className="text-name">New level </span></Button>

                                </Nav>
                            </div>
                        
                            <div className="col-lg-1 search-icon"><img src={searchImg} alt="home-img" width="30" height="30" /></div>
                            <div className="col-lg-7 active-cyan active-cyan-2">
                                <input id="skill"
                                    className="form-control"
                                    type="text"
                                    placeholder="Search with skill"
                                    icon="search"
                                    onChange={this.onChangeSkill}
                                />
                            </div>
                        </div>
                    </div>

                    <h6 className="border-bottom border-gray pb-2 mb-0 text-name ">List of level for skills</h6>
                    <div className="row d-flex flex-row py-4">

                        <div className="w-100 px-4 py-2 d-flex flex-row flex-wrap align-items-center justify-content-between">
                            <div className="d-flex flex-row align-items-center">

                                <span className={headerClass}>
                                    <span className="text-secondary font-weight-bold">{totalLevels}</span> Levels
                        </span>

                                {currentPage && (
                                    <span className=" pl-4 text-secondary">
                                        Page <span className="font-weight-bold">{currentPage}</span> / <span className="font-weight-bold">{totalPages}</span>
                                    </span>
                                )}

                            </div>
                            <div className="d-flex flex-row  align-items-center">
                                <Pagination totalRecords={totalLevels} pageLimit={8} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                            </div>
                        </div>

                        <div className="border-table  person-list">

                            <table className="table table-hover">
                                <thead className="thead-light">
                                    <tr>
                                        <th></th>
                                        <th>Skill</th>
                                        <th>Level</th>
                                        <th>Description of level</th>
                                    </tr>

                                </thead>
                                <tbody className="">
                                    {filterDescriptionLevel}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            );

        }



        else {
            var notAdmin = (
                < NotAdminPage />
            )
        }

        return (

            <>
                {isAdmin || notAdmin}
            </>
        )



    }

}
export default WithAuth(ListLevel)