import React, { Component } from 'react';
import axios from 'axios';
import { Button } from 'react-bootstrap';



class DetailLevel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            skills: [],
            name_skill: '',
            level_description: '',
            level_number: 0,
            skill_description: '',
        }

    }

    componentDidMount() {
        axios.get('/amal/api/DescriptionLevel/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    name_skill: response.data.skill.name_skill,
                    level_number: response.data.level_number,
                    level_description: response.data.level_description,
                    description_skill: response.data.skill.description_skill
                });

            })
            .catch(function (error) {
                console.log(error);
            })

        axios.get("/amal/api/Skill")
            .then(response => {
                const skills = response.data;
                this.setState({ skills });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    render() {

        return (

            <div className="jumbotron  mt-1">
                <h6 className="border-bottom border-gray">Detail of level for skill</h6>
                <div className="pt-3"></div>
                <table className='table table-borderless bg-light rounded'>
                    <tbody  className="">
                        <tr>
                            <td className="text-muted detail-level">Skill name :</td>
                            <td className="small">

                                {this.state.name_skill}
                            </td>
                        </tr>
                        <tr>
                            <td className="text-muted detail-level ">Level of skill:</td>
                            <td>

                                {this.state.level_number}
                            </td>
                        </tr>

                        <tr>
                            <td className="text-muted detail-level">Description of skill:</td>
                            <td className="small">
                                {this.state.description_skill}
                            </td>
                        </tr>
                        <tr>
                            <td className="text-muted detail-level">Description of level:</td>
                            <td className="small">
                                {this.state.level_description}
                            </td>
                        </tr> 
                    </tbody>
                </table>
                <div className="mt-5">
                                <Button href="/empty.levelTable" variant="info" size="bg" block>
                                        Return
                                </Button>
                        </div>  
            </div>
        )
    }
}
export default DetailLevel
