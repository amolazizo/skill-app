import React, { Component } from 'react';
import axios from 'axios';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormErrors from '../form.errors';
import { Link } from 'react-router-dom';

class CreateLevel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            level_description: '',
            level_number: 0,
            formErrors: { level_number: '', level_description: 0, selectedSkillId: -1},
            levelValid: false,
           // skillValid:false,
            levelDescriptionValid: false,
            formValid: false,
            skills: [],
            selectedSkillId: -1,

        };
    }

    componentDidMount() {
        axios.get("/amal/api/Skill")
            .then(response => {
                console.log(response.data);
                this.setState({ skills: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,

        });
    };

    CreateDescriptionLevel() {

        const descriptionLevel = {
            level_description: this.state.level_description,
            level_number: this.state.level_number,
            id_skill: this.state.selectedSkillId,
        };

        const url = `${API_URL}/DescriptionLevel`;

        axios.post(url, descriptionLevel)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.levelTable');


            }).catch((err) => {
                console.log(err);
            });
    }


    handleDescriptionLevelInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }


    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let levelDescriptionValid = this.state.levelDescriptionValid;
      //  let skillValid = this.state.selectedSkillId;
        let levelValid = this.state.levelValid;

        switch (fieldName) {
            case 'level_number':
                levelValid = value.length >= 0 ;
                fieldValidationErrors.level = levelValid ? '' : ' is required';
                break;
            case 'level_description':
                levelDescriptionValid = value.length >= 1;
                fieldValidationErrors.level_description = levelDescriptionValid ? '' : ' is too short, this field is required ';
                break;
          /*  case 'selectedSkillId':
                skillValid = value.length >= -1;
                fieldValidationErrors.selectedSkillId= skillValid ? '' : '  is required ';
                break;  */  
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            levelValid: levelValid,
          //  skillValid: skillValid,
            levelDescriptionValid: levelDescriptionValid
        },
            this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.levelValid && this.state.levelDescriptionValid && this.state.selectedSkillId });
    }

    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }


    handleCreateDescriptionLevel = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: 'You are sure ?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.CreateDescriptionLevel(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };


    render() {

        const skillsOptions = this.state.skills.map(function (skill) {
            return (
                <option key={skill.id} value={skill.id}>{skill.name_skill}</option>
            );
        });

        return (
            <div className="jumbotron ">
                <FormErrors formErrors={this.state.formErrors} />

                <h6 className="border-bottom border-gray">Add new level for skill</h6>
                <div className="pt-3"></div>


                <form onSubmit={this.handleCreateDescriptionLevel}>

                    <table className='table table-borderless'>
                        <tbody>
                            <tr>
                                <td className="text-muted detail-level"><strong className="text-gray-dark">Level:</strong></td>
                                <td className="small person-col">
                                    <input  type="number"
                                        min="0"
                                        max="4"
                                        placeholder="Enter number level"
                                        className="form-control"
                                        name="level_number"
                                        value={this.state.level_number}
                                        onChange={(event) => this.handleDescriptionLevelInput(event)}
                                    />

                                </td>

                            </tr>

                            <tr>
                                <td className="text-muted  detail-level"><strong className="text-gray-dark">Skill name:</strong></td>
                                <td className="person-col">
                                    <select
                                        onChange={(event) => this.handleDescriptionLevelInput(event)}
                                        className='form-control'
                                        name="selectedSkillId"
                                        value={this.state.selectedSkillId}>
                                        <option value="-1">Select skill...</option>
                                        {skillsOptions}
                                    </select>
                                </td>
                            </tr>




                            <tr>
                                <td className="text-muted"><strong className="d-block text-gray-dark">Description of level:</strong></td>
                                <td className="small">
                                    <p></p>
                                    <textarea
                                        id="comment" rows="8"
                                        placeholder="Enter the description of skill ..."
                                        className="form-control"
                                        name="level_description"
                                        value={this.state.level_description}
                                        onChange={(event) => this.handleDescriptionLevelInput(event)}

                                    />
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td className="float-right">
                                    <button
                                        type="submit"
                                        className="btn btn-info"
                                        disabled={!this.state.formValid}>Add level</button>
                                </td>
                                <td className="float-right px-2">
                                <Link to="/empty.LevelTable" className="btn btn-info">
                                        Return
                                </Link>
                                </td> 
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>

        );
    }


}
export default CreateLevel;

