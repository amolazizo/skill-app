import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { Link } from 'react-router-dom';



class EditLevel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            level_number: 0,
            level_description: '',
            id_skill: 0,
            name_skill:"",
        }
    }

    componentDidMount() {
        axios.get('/amal/api/DescriptionLevel/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    level_number: response.data.level_number,
                    level_description: response.data.level_description,
                    id_skill: response.data.id_skill,
                    name_skill: response.data.skill.name_skill
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }


    onChangelevelNumber = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            level_number: e.target.value
        });
    }

    onChangeLevelDescription = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            level_description: e.target.value
        });
    }


    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,
        });
    };

    editLevel() {

        const level = {
            level_number: this.state.level_number,
            level_description: this.state.level_description,
            id_skill: this.state.id_skill,

        };

        const url = `${API_URL}/DescriptionLevel/` + this.props.match.params.id;

        axios.put(url, level)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.levelTable');

            }).catch((err) => {
                console.log(err);
            });
    }

    handleEditLevel = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: ' You want to save this change?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.editLevel(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };

    render() {

        return (

            <div className="jumbotron  mt-1">
                <h6 className="border-bottom border-gray">Make changes to the Level of skill:</h6>
                <div className="pt-3"></div>
                <form onSubmit={this.handleEditLevel}>

                    <div className="row skill-margin">
                        <div className="col-xl-4 text-muted"><strong className="text-gray-dark">Skill name:</strong>
                        </div>

                        <div className="small col-xl-7">
                            <input 
                            type="text"
                            value={this.state.name_skill}
                            />
                        </div>
                        <div className="col-xl-1"></div>
                    </div>



                    <div className="row skill-margin">
                        <div className="text-muted col-xl-4"><strong className="d-block text-gray-dark">Level:</strong>
                        </div>
                        <div className="small col-xl-7">
                            <input type="number"
                                min="0"
                                max="4"
                                required="required"
                                placeholder="Enter number level"
                                className="form-control"
                                name="level_number"
                                value={this.state.level_number}
                                onChange={this.onChangelevelNumber}
                            />
                        </div>
                        <div className="col-xl-1"></div>
                    </div>

                    <div className="row skill-margin">
                        <div className="text-muted col-xl-4"><strong className="d-block text-gray-dark">Level description:</strong>
                        </div>
                        <div className="small col-xl-7">
                            <textarea
                                className="formcontrol" id="comment" rows="8"
                                required="required"
                                value={this.state.level_description}
                                onChange={this.onChangeLevelDescription}

                            />
                        </div>
                        <div className="col-xl-1"></div>
                    </div>


                    <div className="row skill-margin">
                        <div className="col-xl-11 float-righ">
                            <div className="float-right px-2">
                                <button
                                    type="submit"
                                    className="btn btn-info"
                                    required="required"
                                >Update</button>
                            </div>
                            <div className="float-right px-2">
                                <Link to="/empty.levelTable" className="btn btn-info">
                                    Return
                                </Link>
                            </div>
                        </div>
                        <div className="col-xl-1"></div>

                    </div>



                </form >
            </div >


        )
    }

}




export default EditLevel