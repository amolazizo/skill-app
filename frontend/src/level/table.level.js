import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import updateImg from '../pictures/update-img.png';
import deleleImg from '../pictures/delete-img.png';
import readMoreImg from '../pictures/read-more-blue.png'
import SweetAlert from 'react-bootstrap-sweetalert';




class TableLevel extends Component {

    constructor(props) {
        super(props);
        //	this.onDeleteCustomer = this.onDeleteCustomer.bind(this);
        this.state = {
            alert: null

        };

    }



    componentDidMount() {
        axios.get('/amal/api/DescriptionLevel/' + this.props.tab_level.id)

            .catch(function (error) {
                console.log(error);
            })
    }


    onDeleteLevel() {

        axios.delete('/amal/api/DescriptionLevel/' + this.props.tab_level.id)
        this.hideAlert();
        

    }


    alertDelete() {

        const getAlert = () => (

            <SweetAlert
                warning
                showCancel
                confirmBtnText="Yes, delete it!"
                confirmBtnBsStyle="danger"
                title="Are you sure!"
                onConfirm={() => this.onDeleteLevel()}
                onCancel={() => this.hideAlert()}>
                You want to delete this level?
      </SweetAlert>
        );

        this.setState({
            alert: getAlert()

        });


    }

    hideAlert() {
        console.log('Hiding alert...');
        this.setState({
            alert: null
        });
    }

    render() {
        return (
     <tr>
        <td>{this.props.tab_level.skill.name_skill}</td>
        <td>{this.props.tab_level.level_number}</td>
        <td>{this.props.tab_level.level_description}</td>
              <td className="d-flex align-items-center">
              <Link to={"detail.level/"+this.props.tab_level.id} className="align-middle"><img src={readMoreImg} alt="readmore-img" width="20" height="20" className="button-border" /></Link>&nbsp;&nbsp;
	                <Link to={"edit.level/"+this.props.tab_level.id} className="align-middle"><img src={updateImg} alt="update-img" width="20" height="20" className="button-border" /></Link>
                    <button onClick={() => this.alertDelete()} className="button-border">
                     <i className="algin-middle"></i><img src={deleleImg} alt="delete-img" width="23" height="23" />
	                 </button>
                     {this.state.alert}
                </td>

                
            </tr>



        );
    }
}

export default TableLevel;
