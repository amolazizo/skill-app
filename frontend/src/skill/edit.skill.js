import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { Link } from 'react-router-dom';



class EditSkill extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name_skill: '',
            description_skill: '',
        }
    }

    componentDidMount() {
        axios.get('/amal/api/Skill/' + this.props.match.params.id)
            .then(response => {
                console.log(response.data.name_skill);
                this.setState({
                    name_skill: response.data.name_skill,
                    description_skill: response.data.description_skill
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }


    onChangeNameSkill = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            name_skill: e.target.value
        });
    }

    onChangeDescriptionSkill = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            description_skill: e.target.value
        });
    }


    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,
        });
    };

    editSkill() {

        const skill = {
            name_skill: this.state.name_skill,
            description_skill: this.state.description_skill
        };

        const url = `${API_URL}/Skill/` + this.props.match.params.id;

        axios.put(url, skill)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.skillTable');

            }).catch((err) => {
                console.log(err);
            });
    }

    handleEditSkill = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: ' You want to save this change?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.editSkill(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };

    render() {

        return (

            <div className="jumbotron  mt-1">
                <h6 className="border-bottom border-gray">Make changes to the skill:</h6>
                <div className="pt-3"></div>
                <form onSubmit={this.handleEditSkill}>

                    <div className="row skill-margin">
                        <div className="col-xl-4 text-muted"><strong className="text-gray-dark">Skill name:</strong>
                        </div>

                        <div className="small col-xl-7">
                            <input
                                type="text"
                                required="required"
                                placeholder="Enter the name"
                                value={this.state.name_skill}
                                onChange={this.onChangeNameSkill}
                            />


                        </div>
                        <div className="col-xl-1"></div>
                        </div>
                        <div className="row skill-margin">
                            <div className="text-muted col-xl-4"><strong className="d-block text-gray-dark">Skill description:</strong>
                            </div>
                            <div className="small col-xl-7">
                            <textarea
                                className="formcontrol" id="comment" rows="8"
                                required="required"
                                value={this.state.description_skill}
                                onChange={this.onChangeDescriptionSkill}

                            />
                        </div>
                        <div className="col-xl-1"></div>
                            </div>
                      

                        <div className="row skill-margin">
                        <div className="col-xl-11 float-righ">
                            <div className="float-right px-2">
                            <button 
                            type="submit" 
                            className="btn btn-info"
                            required="required"
                            >Update skill</button> 
                            </div>
                            <div className="float-right px-2">
                                <Link to="/empty.skillTable" className="btn btn-info">
                                        Return
                                </Link>
                        </div> 
                        </div>
                        <div className="col-xl-1"></div>

                    </div>
                </form >
            </div >

         
      )
    }

}




export default EditSkill