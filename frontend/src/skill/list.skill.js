import React, { Component } from 'react';
import axios from 'axios';
import WithAuth from '../withAuth';
import { Nav, Button} from 'react-bootstrap';
//import { Input } from 'mdbreact';
import { Link } from 'react-router-dom';

import updateImg from '../pictures/update-img-grey.png';
import deleleImg from '../pictures/delete-img-grey.png';
import SweetAlert from 'react-bootstrap-sweetalert';

import NotAdminPage from '../not.admin.page';
import searchImg from '../pictures/search.png';
import addImg from '../pictures/add-img-grey.png';



import Pagination from '../pagination.js';


class ListSkill extends Component {

   constructor(props) {
    super(props);
    this.state = { 
        is_admin: false,
        allSkills: [],
        currentSkills: [],
        currentPage: null,
        totalPages: null,
        searchSkill:"",
        alert: null
         }
    }

   componentWillMount() {
        console.log(this.props.person.identity)
        var id_google = (this.props.person.identity)
        axios.get('/amal/api/Person/' + id_google)
            .then(response => {
                console.log("Admin :" + response.data.is_admin);
                this.setState({
                    is_admin: response.data.is_admin,
                });

            })
            .catch(function (error) {
                console.log(error);
            })

    }
         componentDidMount() {
            axios.get('/amal/api/Skill')
                .then(response => {
                    console.log(response.data);
                    this.setState({ allSkills: response.data });
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
  
   onPageChanged = data => {
    const { allSkills } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentSkills = allSkills.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentSkills, totalPages });
  }


  onchange = e => {
    this.setState({
         searchSkill: e.target.value
        });
  };



hideAlert() {
  console.log('Hiding alert...');
  this.setState({
      alert: null
  });
}



renderSkills =  skill => {
    return (
      <>
           <tr>
             <td className="d-flex align-items-center">
	                <Link to={"edit.skill/"+ skill.id} className="align-middle"><img src={updateImg} alt="update-img" width="18" height="18" className="button-border" /></Link>
                    <button onClick={()  => {
                                              const getAlert = () => (
                                                  <SweetAlert
                                                      warning
                                                      showCancel
                                                      confirmBtnText="Yes, delete it!"
                                                      confirmBtnBsStyle="danger"
                                                      title="Are you sure!"
                                                      onConfirm={() => { 
                                                        axios.delete('/amal/api/Skill/' + skill.id)
                                                        this.hideAlert();

                                                      }
                                                      }
                                                      onCancel={() => this.hideAlert()}>
                                                      You will not be able to recover this skill
                                            </SweetAlert>
                                              );

                                              this.setState({
                                                  alert: getAlert()

                                              });
                                            }
                    
                    }  className="button-border">
                     <i className="fa fa-trash algin-middle" aria-hidden="true"></i><img src={deleleImg} alt="delete-img" width="20" height="20" />
	                 </button>
                     {this.state.alert}
                </td>
              <td>
              {skill.name_skill.substring(0, 15)} {skill.name_skill.length > 15 && "..."}
              </td>
              <td>
                {skill.description_skill}
              </td>
              </tr>
              </>
    );
  };



    render() {
        const {
            allSkills,
            currentSkills,
            currentPage,
            totalPages
          } = this.state;
          const totalSkills = allSkills.length;
          if (totalSkills === 0) return null;
      
          const headerClass = [
            "text-dark py-2 pr-4 m-0",
            currentPage ? "border-gray border-right" : ""
          ]
            .join(" ")
            .trim();

            const { searchSkill } = this.state;

            const filteredSkills = currentSkills.filter(skill => {
                console.log(skill.name_skill);
              return skill.name_skill.toLowerCase().indexOf(searchSkill.toLowerCase()) !== -1;
            });
    
    
            const filterSkill = filteredSkills.map(skill => {
              return this.renderSkills(skill);
              });
        

            if (this.state.is_admin === true) 
                {
                    var isAdmin = (
                        <div>

        <div className="jumbotron  mt-1">
            <div className="row">
                <div className="col-lg-4">
                <Nav className="mr-auto">
                <Button href="/create.skill"  className="btn btn-light " ><img src={addImg} alt="home-img" width="25" height="25" /> <span className="text-name"> New skill </span></Button>   
                </Nav>
                </div>
            <div className="col-lg-1 search-icon"><img src={searchImg} alt="home-img" width="30" height="30" /></div>
              <div className="col-lg-7 active-cyan active-cyan-2">
                <input
                className="form-control"
                type="text"
                  placeholder="Search Skill"
                  icon="search"
                  onChange={this.onchange}
                />
              </div>
            </div>
            </div>
                <h6 className="border-bottom border-gray pb-2 mb-0 text-name ">List of skills</h6>
                <div className="row d-flex flex-row py-4">

                <div className="w-100 px-4 py-2 d-flex flex-row flex-wrap align-items-center justify-content-between">
                    <div className="d-flex flex-row align-items-center">

                    <span className={headerClass}>
                        <span className="text-secondary font-weight-bold">Total: </span><span className="text-secondary font-weight-bold">{totalSkills}</span> Skills
                    </span>

                    { currentPage && (
                        <span className=" pl-4 text-secondary">
                        Page <span className="font-weight-bold">{ currentPage }</span> / <span className="font-weight-bold">{ totalPages }</span>
                        </span>
                    ) }

                    </div>
                    <div className="d-flex flex-row  align-items-center">
                    <Pagination totalRecords={totalSkills} pageLimit={8} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                    </div>
                </div>
        
          <div className="container">

            
        <div className="border-table  person-list">
        <table className="table table-hover">
            <thead className="thead-light">
                <tr>
                    <th></th>
                    <th>Skill</th>
                    <th>Description of skill</th>
                </tr>

            </thead>
            <tbody className="">
              {filterSkill}
              </tbody>
              </table>
            </div>
          </div>
                </div>
              </div>
            );
        
          }


            
        else {
            var notAdmin = (
                < NotAdminPage />
            )
        }

        return (

          <>
              

                    {isAdmin || notAdmin}
                </>
        )


        
        }
      
}
export default WithAuth(ListSkill)