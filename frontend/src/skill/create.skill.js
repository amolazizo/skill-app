import React, { Component } from 'react';
import axios from 'axios';
import { API_URL } from '../Config.js';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import 'react-confirm-alert/src/react-confirm-alert.css';
import FormErrors from '../form.errors';
import { Link } from 'react-router-dom';
import VerticalNavBarre from '../vertical.nav.barre';

 
class CreateSkill extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name_skill: '',
            description_skill: '',
            formErrors: { name_skill: ''},
            nameValid: false,
           // descriptionValid: false,
            formValid: false
        };
    }

    notify = () => {
        toast.success("Success !", {
            position: toast.POSITION.TOP_RIGHT,

        });
    };

    createSkill() {

        const skill = {
            name_skill: this.state.name_skill,
            description_skill: this.state.description_skill
        };

        const url = `${API_URL}/Skill`;

        axios.post(url, skill)
            .then((result) => {
                console.log(result);
                this.notify();
                this.props.history.replace('/empty.mySkillTable');


            }).catch((err) => {
                console.log(err);
            });
    }


    handleSkillInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }


    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let nameValid = this.state.nameValid;
        //let descriptionValid = this.state.descriptionValid;

        switch (fieldName) {
            case 'name_skill':
                nameValid = value.length > 0;
                fieldValidationErrors.name = nameValid ? '' : ' is too short, min 1 character, requis';
              /*  break;
            case 'description_skill':
                descriptionValid = value.length >= 1;
                fieldValidationErrors.description = descriptionValid ? '' : ' is too short ';*/
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            nameValid: nameValid
            //descriptionValid: descriptionValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.nameValid });
    }

    errorClass(error) {
        return(error.length === 0 ? '' : 'has-error');
    }   


    handleCreateSkill = (event) => {
        event.preventDefault();
        confirmAlert({
            title: 'Confirmation',
            message: 'You are sure ?',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => this.createSkill(),

                },
                {
                    label: 'Non',
                    onClick: () => console.log('Click No')
                }
            ]
        })
    };


    render() {

        return (
      
                <>
                    <div className="col-lg-3 bg-info rounded">
                            <VerticalNavBarre />
                    </div>
                    <div className="col-lg-9 pl-4">
                    <div className="jumbotron">
                    <FormErrors formErrors={this.state.formErrors} />
            
                <h6 className="border-bottom border-gray pb-2 mb-0 text-name">Add new skill</h6><br></br><br></br>
                <form onSubmit={this.handleCreateSkill}>

                    <table className='table table-borderless'>
                        <tbody>
                            <tr>
                                <td className="text-muted detail-level"><strong className="text-gray-dark">Skill name:</strong></td>
                                <td className="small person-col">
                                    <input
                                        type="text"
                                        placeholder="Enter the name"
                                        className="form-control"
                                        name="name_skill"
                                        value={this.state.name}
                                        //onChange={this.onChangeNameSkill}
                                        onChange={(event) => this.handleSkillInput(event)}
                                    />

                                </td>

                            </tr>

                            <tr>
                                <td className="text-muted detail-level"><strong className="d-block text-gray-dark">Skill description:</strong></td>
                                <td className="small person-col">
                                    <textarea
                                        id="comment" rows="8"
                                        placeholder="Enter the description of skill ..."
                                        className="form-control"
                                        name="description_skill"
                                        value={this.state.description}
                                        //onChange={this.onChangeDescriptionSkill}
                                        onChange={(event) => this.handleSkillInput(event)}

                                    />
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td className="float-right">
                                    <button
                                        type="submit"
                                        className="btn btn-info"
                                        disabled={!this.state.formValid}>Add skill</button>
                                </td>
                                <td className="float-right px-2">
                                <Link to="/empty.mySkillTable" className="btn btn-info">
                                        Return
                                </Link>
                                </td> 

                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            </div>
            </>
        

        );
    }


}
export default CreateSkill;

/*

import React, { Component } from 'react';
import axios from 'axios';



class CreateSkill extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name_skill: '',
            description_skill: ''

        }
    }

    onChangeNameSkill = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            name_skill: e.target.value
        });
    }

    onChangeDescriptionSkill = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            description_skill: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        const skill = {
            name_skill: this.state.name_skill,
            description_skill: this.state.description_skill
        };

        axios.post('/amal/api/Skill', skill)
            .then(result=> console.log(result.data));
        this.setState({
            name_skill: '',
            description_skill: ''

        })

    }

    render() {
        return (

            <div>
                    <h6 class="border-bottom border-gray pb-2 mb-0">Add new skill</h6>
                    <div class="pt-3"></div>
                <form onSubmit={this.onSubmit}>

                    <table className='table table-borderless'>
                        <tbody>
                            <tr>
                            <td class="text-muted"><strong class="text-gray-dark">Skill name:</strong></td>
                            <td class="small">
                                    <input
                                        type="text"
                                        placeholder="Enter the name"
                                        value={this.state.name_skill}
                                        onChange={this.onChangeNameSkill}
                                    />

                                </td>

                            </tr>

                            <tr>
                            <td class="text-muted"><strong class="d-block text-gray-dark">Skill description:</strong></td>
                                <td class="small">
                                    <textarea
                                        class="formcontrol" id="comment" rows="8"
                                        placeholder="Enter the description of skill ..."
                                        value={this.state.description_skill}
                                        onChange={this.onChangeDescriptionSkill}

                                    />
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td class="float-right">
                                    <input
                                        type="submit"
                                        value="Save skill"
                                        class="btn btn-info"
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>

        );
    }
}
export default CreateSkill;



*/

