import React from 'react';



const FormErrors = ({formErrors}) =>

  <div>
    {Object.keys(formErrors).map((fieldName, i) => {
      if(formErrors[fieldName].length > 0 ){
        return (
            <div className="formErrors rounded bg-light">
            <ul>
          <li className="textErrors" key={i}>{fieldName} {formErrors[fieldName]}</li>
          </ul>         
           </div>
        )        
      } else {
        return '';
      }
    })}
   
  </div>

export default (FormErrors)