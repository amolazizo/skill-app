import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class RegistrationError extends Component {

    render() {
return (
<div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
          <div>
                 <h1>an error occurred while creating your account!</h1><br>
                </br>
                <p>Please try again clicking<Link to='/register.person'className="van-link">here</Link></p>
          </div>
        
          </div>
        </div>
            </div>
)
    }

}

export default RegistrationError;